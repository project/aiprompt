CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation and usage
 * Development

INTRODUCTION
------------

Enables to engineer AI prompts. For example, AI assistants character and knowledge base.

For more information visit project page https://drupal.org/project/aiprompt

REQUIREMENTS
------------

 * None

RECOMMENDED MODULES
------------

For recommended modules visit project page https://drupal.org/project/aiprompt

INSTALLATION AND USAGE
----------------------

- Follow the standard Drupal installation instructions:
  https://www.drupal.org/docs/extending-drupal/installing-modules

- Enable one of the prompt storage modules: "Content" storage enable site users to engineer prompts, versus "configuration" storage enables administrators to configure site-wide prompts which can be exported/imported by modules.

- **Content storage**
  - Create and configure prompt type under /admin/structure/aiprompt/types
  - Create new prompt /admin/content/aiprompt

- **Configuration storage**
  - Goto to create new prompt /admin/config/aiprompt

- To utilize the prompt follow instruction in the section "development" below, or install an implementing module (like [AI chat user inferface](https://www.drupal.org/project/aichat))

DEVELOPMENT
-----------

- Developers can provide new prompt segment plugins. Examples are within module codebase in Plugin/AIPrompt directories for module and each sub-module.

- To utilize prompt output:


```php
// choose storage type
$storage_type = 'aiprompt'; // content storage
// or
$storage_type = 'aiprompt_config'; // configuration storage

// load prompt
$entity = \Drupal::entityTypeManager()->getStorage($storage_type)->load('[enter_prompt_id]');

// Get output string
$string = $entity->toString();

// Use the output string (prints the string)
\Drupal::messenger()->addStatus($string);
```
