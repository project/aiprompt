(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.aiprompt_engineeringForm = {
    attach: function (context, settings) {

      if ($('body.aiprompt-engineering-jsloaded').length) {
        return;
      }
      $('body').addClass('aiprompt-engineering-jsloaded');

      // Act when user click segment status operation link
      // @see. https://www.drupal.org/project/aiprompt/issues/3436593
      $('body', context).on('click', 'tr a.segment-status-link', function(e) {
        e.preventDefault();
        let $checkbox = $(this).closest('tr').find('input.segment-status-checkbox');
        let $submit = $(this).closest('form').find('[data-drupal-selector="edit-actions"] [data-drupal-selector="edit-submit"]');
        $checkbox.prop('checked', !$checkbox.prop('checked'));
        $submit.click();
      });

    }
  }

})(jQuery, Drupal);
