<?php

namespace Drupal\aiprompt_config\Form;

use Drupal\aiprompt\AIPromptArgumentPluginManager;
use Drupal\aiprompt\AIPromptSegmentPluginManager;
use Drupal\aiprompt\Traits\AIPromptCommonEngineeringForm;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Url;
use Drupal\Core\Render\Markup;

/**
 * Form for a Prompt configuration entity.
 */
class AIPromptConfigForm extends EntityForm {

  use AIPromptCommonEngineeringForm;

  /**
   * The AI Prompt Plugin manager.
   *
   * @var \Drupal\aiprompt\AIPromptSegmentPluginManager
   */
  protected $aipromptSegmentPluginManager;

  /**
   * The AI Prompt argument plugin manager.
   *
   * @var \Drupal\aiprompt\AIPromptArgumentPluginManager
   */
  protected $aipromptArgumentPluginManager;

  /**
   * Constructs a new PromptConfigurationForm.
   *
   * @param \Drupal\aiprompt\AIPromptSegmentPluginManager $aiprompt_plugin_manager
   *   The AI Prompt Plugin manager.
   */
  public function __construct(AIPromptSegmentPluginManager $aiprompt_segment_plugin_manager,
                              AIPromptArgumentPluginManager $aiprompt_argument_plugin_manager) {
    $this->aipromptSegmentPluginManager = $aiprompt_segment_plugin_manager;
    $this->aipromptArgumentPluginManager = $aiprompt_argument_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.aiprompt'),
      $container->get('plugin.manager.aiprompt_argument')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $this->entity_type = $this->entity->getEntityTypeId();

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#description' => $this->t("Label for the Prompt"),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\aiprompt_config\Entity\AIPromptConfig::load',
      ],
      '#disabled' => TRUE,
    ];

    $form['notes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Administrative notes'),
      '#default_value' => $this->entity->getNotes(),
      '#description' => $this->t('Notes about this prompt.'),
    ];

    $form = $this->buildSegmentsForm($form, $form_state);
    $form = $this->buildArgumentsForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $this->entity->setNotes($form_state->getValue('notes'));
    $this->entity->updateSegmentsFromSubmittedData($form_state->getValue('segments_table'));
    $status = $this->entity->save();

    if ($status) {
      $this->messenger()->addMessage($this->t('Saved the %label AI Prompt Configuration.', [
        '%label' => $this->entity->label(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The %label AI Prompt Configuration was not saved.', [
        '%label' => $this->entity->label(),
      ]));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
