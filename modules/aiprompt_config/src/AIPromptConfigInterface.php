<?php

namespace Drupal\aiprompt_config;

use Drupal\aiprompt\AIPromptInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a AI prompt configuration entity.
 */
interface AIPromptConfigInterface extends ConfigEntityInterface, AIPromptInterface {

  /**
   * Gets the Administrative notes.
   *
   * @return string
   */
  public function getNotes(): ?string;

  /**
   * Sets the Administrative notes.
   *
   * @param string $notes
   */
  public function setNotes(string $notes): void;

}
