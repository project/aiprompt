<?php

namespace Drupal\aiprompt_config\Entity;

use Drupal\aiprompt\Traits\AIPromptCommons;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * @ConfigEntityType(
 *   id = "aiprompt_config",
 *   label = @Translation("AI Prompt"),
 *   handlers = {
 *     "list_builder" = "Drupal\aiprompt_config\AIPromptConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\aiprompt_config\Form\AIPromptConfigCreateForm",
 *       "edit" = "Drupal\aiprompt_config\Form\AIPromptConfigForm",
 *       "delete" = "Drupal\aiprompt_config\Form\AIPromptConfigDeleteForm",
 *     }
 *   },
 *   config_prefix = "aiprompt_config",
 *   admin_permission = "administer aiprompt configurations",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/aiprompt/add",
 *     "edit-form" = "/admin/config/aiprompt/{aiprompt_config}/edit",
 *     "delete-form" = "/admin/config/aiprompt/{aiprompt_config}/delete",
 *     "collection" = "/admin/config/aiprompt",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "segments",
 *     "notes",
 *     "arguments",
 *   },
 * )
 */
class AIPromptConfig extends ConfigEntityBase {

  use AIPromptCommons;

  /**
   * The AI Prompt Configuration ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The AI Prompt Configuration label.
   *
   * @var string
   */
  protected $label;

  /**
   * The AI Prompt segments.
   *
   * @var array
   */
  protected $segments = [];

  protected $segment_objects = [];

  /**
   * The Administrative notes for the AI Prompt Configuration.
   *
   * @var string
   */
  protected $notes;

  /**
   * The AI Prompt arguments.
   *
   * @var array
   */
  protected $arguments = [];

  protected $argument_objects = [];

  /**
   * For tracking the state of the object.
   *
   * @var array
   */
  protected $object_state;

  /**
   * The AI prompt segment plugin manager.
   *
   * @var \Drupal\aiprompt\AIPromptSegmentPluginManager
   */
  protected $aipromptSegmentPluginManager;

  /**
   * The AI prompt argument plugin manager.
   *
   * @var \Drupal\aiprompt\AIPromptArgumentPluginManager
   */
  protected $aipromptArgumentPluginManager;

  /**
   * {@inheritdoc}
   */
  public function getNotes(): ?string {
    return $this->notes;
  }

  /**
   * {@inheritdoc}
   */
  public function setNotes(string $notes): void {
    $this->notes = $notes;
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentsData(): array {
    return $this->segments;
  }

  /**
   * {@inheritdoc}
   */
  public function setSegmentsData(array $segments): void {
    $this->segments = $segments;
  }

  /**
   * {@inheritdoc}
   */
  public function setSegmentData($segment, $machine_name = NULL) {
    if (empty($machine_name)) {
      $machine_name = $this->generateSegmentMachineName();
    }
    if (empty($this->segments)) {
      $this->segments = [];
    }
    $this->segments[$machine_name] = $segment;
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentData($machine_name = NULL) {
    $segment = !empty($machine_name) ? $this->segments[$machine_name] : [];
    $segment['settings']['aiprompt_id'] = $this->id();
    return $segment;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSegmentData($machine_name) {
    unset($this->segments[$machine_name]);
  }

  /**
   * {@inheritdoc}
   */
  public function getArgumentsData(): array {
    return $this->arguments;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgumentData(string $name = NULL): array {
    return $this->arguments[$name] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateArgumentData(array $data, string $name): void {
    $this->arguments[$name] = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteArgumentData(string $name): void {
    unset($this->arguments[$name]);
  }
}
