<?php

namespace Drupal\aiprompt_config\Plugin\AIPromptSegment;

use Drupal\aiprompt\Annotation\AIPromptSegment;
use Drupal\aiprompt\Plugin\PromptEntitySegmentBase;
use Drupal\aiprompt_config\Entity\AIPromptConfig;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides AI Prompt plugin.
 *
 * @AIPromptSegment(
 *   id = "config_prompt",
 *   label = @Translation("Prompt (config storage)"),
 * )
 */
class ConfigPrompt extends PromptEntitySegmentBase {

  protected $entity_type = 'aiprompt_config';

  /**
   * Helper function to get prompt.
   */
  public function getPrompt() {
    $entity_id = $this->getPluginConfigValueTokenized('entity_id');
    if (!empty($entity_id)) {
      return AIPromptConfig::load($entity_id);
    }
    return NULL;
  }

}
