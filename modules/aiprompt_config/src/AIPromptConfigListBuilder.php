<?php

namespace Drupal\aiprompt_config;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of AI Prompt entities.
 */
class AIPromptConfigListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build['add_link'] = [
      '#type' => 'link',
      '#title' => $this->t('Create new AI prompt'),
      '#url' => Url::fromRoute('entity.aiprompt_config.add_form'),
      '#attributes' => ['class' => ['button', 'button--primary']],
      '#weight' => -10,
    ];
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['notes'] = $this->t('Notes');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\aiprompt_config\Entity\AIPromptConfig */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['notes'] = $entity->getNotes(); // @TODO. Display multiline
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    // removes destination parameter
    if (!empty($operations['edit'])) {
      $operations['edit']['url'] = $entity->toUrl('edit-form');
    }

    return $operations;
  }
}

