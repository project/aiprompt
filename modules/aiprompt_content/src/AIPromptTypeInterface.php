<?php

namespace Drupal\aiprompt_content;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a custom entity bundle.
 */
interface AIPromptTypeInterface extends ConfigEntityInterface {

  /**
   * Returns the description of the bundle.
   *
   * @return string
   *   The description of the bundle.
   */
  public function getDescription(): ?string;

}
