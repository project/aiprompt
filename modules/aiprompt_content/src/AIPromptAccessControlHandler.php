<?php

namespace Drupal\aiprompt_content;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the aiprompt entity.
 */
class AIPromptAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   *
   * Link the activities to the permissions. checkAccess() is called with the
   * $operation as defined in the routing.yml file.
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    // defined in entity type anotation
    $admin_permission = $this->entityType->getAdminPermission();

    if ($account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }

    $permissions = [];

    switch ($operation) {

      case 'use':
        $permissions[] = 'use aiprompt content';
        if ($entity->getOwnerId() === $account->id()) {
          $permissions[] = 'use own aiprompt content';
        }
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'update':
        $permissions[] = 'edit aiprompt content';
        if ($entity->getOwnerId() === $account->id()) {
          $permissions[] = 'edit own aiprompt content';
        }
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');

      case 'delete':
        $permissions[] = 'delete aiprompt content';
        if ($entity->getOwnerId() === $account->id()) {
          $permissions[] = 'delete own aiprompt content';
        }
        return AccessResult::allowedIfHasPermissions($account, $permissions, 'OR');
    }
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {

    $admin_permission = $this->entityType->getAdminPermission();

    if ($account->hasPermission($admin_permission)) {
      return AccessResult::allowed();
    }
    return AccessResult::allowedIfHasPermission($account, 'add aiprompt content');
  }

}
