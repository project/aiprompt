<?php

namespace Drupal\aiprompt_content\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class for determining the admin theme on AI prompt create/edit routes.
 */
class AIPromptThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * The config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs an AIPromptThemeNegotiator object.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route_name = $route_match->getRouteName();
    $use_admin_theme = $this->configFactory->get('node.settings')->get('use_admin_theme');
     return $use_admin_theme && in_array($route_name, [
      'aiprompt_content.aiprompt_add',
      'aiprompt_content.aiprompt_add_form',
      'entity.aiprompt.edit_form',
      'aiprompt.segment.add_form',
      'aiprompt.segment.edit_form',
      'aiprompt.segment.delete_form',
      'aiprompt.argument.add_form',
      'aiprompt.argument.edit_form',
      'aiprompt.argument.delete_form',
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->configFactory->get('system.theme')->get('admin');
  }

}
