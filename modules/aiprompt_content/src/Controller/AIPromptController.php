<?php

namespace Drupal\aiprompt_content\Controller;

use Drupal\aiprompt_content\AIPromptTypeInterface;
use Drupal\aiprompt_content\AIPromptContentInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Request;

class AIPromptController extends ControllerBase {

  /**
   * Displays add  links for available types.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A render array for a list of the entity types that can be added.
   */
  public function add(Request $request) {
    $build = [
      '#theme' => 'entity_add_list',
      '#cache' => [
        'tags' => $this->entityTypeManager()->getDefinition('aiprompt_type')->getListCacheTags(),
      ],
    ];

    $bundles = [];

    $types = $this->entityTypeManager()->getStorage('aiprompt_type')->loadMultiple();

    foreach ($types as $type) {
      $bundles[$type->id()] = [
        'add_link' => Link::fromTextAndUrl($type->label(), Url::fromRoute('aiprompt_content.aiprompt_add_form', ['aiprompt_type' => $type->id()])),
        'label' => $type->label(),
        'description' => $type->getDescription(),
      ];
    }

    $build['#bundles'] = $bundles;
    $build['#add_bundle_message'] = $this->t('No Prompt types available. Please create AI Prompt type first.');

    return $build;
  }

  /**
   * Presents the entity creation form.
   *
   * @param \Drupal\aiprompt_content\AIPromptTypeInterface $aiprompt_type
   *   The aiprompt type to add.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request object.
   *
   * @return array
   *   A form array as expected by
   *   \Drupal\Core\Render\RendererInterface::render().
   */
  public function addForm(AIPromptTypeInterface $aiprompt_type, Request $request) {
    $entity = $this->entityTypeManager()->getStorage("aiprompt")->create([
      'type' => $aiprompt_type->id(),
    ]);
    return $this->entityFormBuilder()->getForm($entity);
  }
  
  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\aiprompt_content\AIPromptTypeInterface $aiprompt_type
   *   The aiprompt type being added.
   *
   * @return string
   *   The page title.
   */
  public function getAddFormTitle(AIPromptTypeInterface $aiprompt_type) {
    return $this->t('Add %type aiprompt', ['%type' => $aiprompt_type->label()]);
  }

  /**
   * Provides the page title for this controller.
   *
   * @param \Drupal\aiprompt\AIPromptContentInterface $aiprompt
   *   The AI prompt being viewed.
   *
   * @return string
   *   The page title.
   */
  public function getAIPromptTitle(AIPromptContentInterface $aiprompt) {
    return $aiprompt->label();
  }
}
