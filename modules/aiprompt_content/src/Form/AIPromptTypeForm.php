<?php

namespace Drupal\aiprompt_content\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * The aiprompt type entity form.
 *
 * @internal
 */
class AIPromptTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $entity = $this->entity;

    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add AI prompt type');
    }
    else {
      $form['#title'] = $this->t('Edit %label custom type', ['%label' => $entity->label()]);
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Provide a label for this type to help identify it in the administration pages."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => '\Drupal\aiprompt_content\Entity\AIPromptType::load'
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#default_value' => $entity->getDescription(),
      '#description' => $this->t('Enter a description for this type.'),
      '#title' => $this->t('Description'),
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $this->protectBundleIdElement($form);
  } 

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = $entity->save();

    $edit_link = $this->entity->toLink($this->t('Edit'), 'edit-form')->toString();
    $logger = $this->logger('aiprompt');
    if ($status == SAVED_UPDATED) {
      $this->messenger()->addStatus($this->t('Custom type %label has been updated.', ['%label' => $entity->label()]));
      $logger->notice('Custom type %label has been updated.', ['%label' => $entity->label(), 'link' => $edit_link]);
    }
    else {
      $this->messenger()->addStatus($this->t('Custom type %label has been added.', ['%label' => $entity->label()]));
      $logger->notice('Custom type %label has been added.', ['%label' => $entity->label(), 'link' => $edit_link]);
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
  }

}
