<?php

namespace Drupal\aiprompt_content\Form;

use Drupal\aiprompt\AIPromptArgumentPluginManager;
use Drupal\aiprompt\AIPromptSegmentPluginManager;
use Drupal\aiprompt\Traits\AIPromptCommonEngineeringForm;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Datetime\TimeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for aiprompt edit forms.
 *
 * @ingroup aiprompt_content
 */
class AIPromptForm extends ContentEntityForm {

  use AIPromptCommonEngineeringForm;

  /**
   * The AI Prompt segment plugin manager.
   *
   * @var \Drupal\aiprompt\AIPromptSegmentPluginManager
   */
  protected $aipromptSegmentPluginManager;

  /**
   * The AI Prompt argument plugin manager.
   *
   * @var \Drupal\aiprompt\AIPromptArgumentPluginManager
   */
  protected $aipromptArgumentPluginManager;

  /**
   * Constructs a new PromptConfigurationForm.
   *
   * @param \Drupal\aiprompt\AIPromptSegmentPluginManager $aiprompt_plugin_manager
   *   The AI Prompt Plugin manager.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info,
                              TimeInterface $time, AIPromptSegmentPluginManager $aiprompt_segment_plugin_manager,
                              AIPromptArgumentPluginManager $aiprompt_argument_plugin_manager) {

    parent::__construct($entity_repository, $entity_type_bundle_info, $time);

    $this->aipromptSegmentPluginManager = $aiprompt_segment_plugin_manager;
    $this->aipromptArgumentPluginManager = $aiprompt_argument_plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.aiprompt'),
      $container->get('plugin.manager.aiprompt_argument')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['actions']['#weight'] = 200;

    $id = $this->entity->id();
    if (!empty($id)) {
      $form = $this->buildSegmentsForm($form, $form_state);
      $form = $this->buildArgumentsForm($form, $form_state);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $element = parent::actions($form, $form_state);
    $entity = $this->entity;

    $account = \Drupal::currentUser();
    $element['delete']['#access'] = $account->hasPermission('delete aiprompt entity');
    $element['delete']['#weight'] = 100;

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array $form, FormStateInterface $form_state) {
    $entity = parent::submit($form, $form_state);
    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $submitted_data = $form_state->getValue('segments_table');
    if (!empty($submitted_data)) {
      $entity->updateSegmentsFromSubmittedData($submitted_data);
    }

    $status = $entity->save();

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created the %label.', [
            '%label' => $entity->label(),
          ]));

        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved the %label.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect("entity.aiprompt.edit_form", ["aiprompt" => $entity->id()]);
  }

}
