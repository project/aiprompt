<?php

namespace Drupal\aiprompt_content\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Data view widget.
 *
 * @FieldWidget(
 *   id = "aiprompt_data_view_widget",
 *   label = @Translation("AI Prompt data view widget"),
 *   field_types = {
 *     "string_long"
 *   }
 * )
 */
class AIPromptDataViewWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    // This widget is only applicable for 'data' fields of AIPrompt entity.
    if ($field_definition->getTargetEntityTypeId() == 'aiprompt' && $field_definition->getName() == 'data') {
        return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = isset($items[$delta]->value) ? $items[$delta]->value : $this->t('Entity does not have data yet');

    $element['value'] = [
      '#type' => 'details',
      '#title' => $this->t('Data'),
      '#open' => FALSE,
      '#markup' => Markup::create('<pre>' . $value . '</pre>')
    ];

    return $element;
  }

}