<?php

namespace Drupal\aiprompt_content\Plugin\AIPromptSegment;

use Drupal\aiprompt\Annotation\AIPromptSegment;
use Drupal\aiprompt\Plugin\PromptEntitySegmentBase;
use Drupal\aiprompt_content\Entity\AIPrompt;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides AI Prompt plugin.
 *
 * @AIPromptSegment(
 *   id = "content_prompt",
 *   label = @Translation("Prompt (content storage)"),
 * )
 */
class ContentPrompt extends PromptEntitySegmentBase {

  protected $entity_type = 'aiprompt';

  /**
   * Helper function to get prompt.
   */
  public function getPrompt() {
    $entity_id = $this->getPluginConfigValueTokenized('entity_id');

    if (!empty($entity_id)) {
      return AIPrompt::load($entity_id);
    }
    return NULL;
  }
}
