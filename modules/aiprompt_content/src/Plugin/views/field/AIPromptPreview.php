<?php

namespace Drupal\aiprompt_content\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler to present a preview of an AI Prompt.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("aiprompt_preview")
 */
class AIPromptPreview extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() { }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\aiprompt_content\Entity\AIPrompt $aiprompt */
    $aiprompt = $values->_entity;
    return $aiprompt->buildPreview();
  }

}