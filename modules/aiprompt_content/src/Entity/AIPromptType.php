<?php

namespace Drupal\aiprompt_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\aiprompt_content\AIPromptTypeInterface;

/**
 * Defines the aiprompt type entity.
 *
 * @ConfigEntityType(
 *   id = "aiprompt_type",
 *   label = @Translation("AI prompt type"),
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\aiprompt_content\Form\AIPromptTypeForm",
 *       "add" = "Drupal\aiprompt_content\Form\AIPromptTypeForm",
 *       "edit" = "Drupal\aiprompt_content\Form\AIPromptTypeForm",
 *       "delete" = "Drupal\aiprompt_content\Form\AIPromptTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "permissions" = "Drupal\user\Entity\EntityPermissionsRouteProviderWithCheck",
 *     },
 *     "list_builder" = "Drupal\aiprompt_content\AIPromptTypeListBuilder"
 *   },
 *   admin_permission = "administer aiprompts",
 *   config_prefix = "aiprompt_type",
 *   bundle_of = "aiprompt",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" = "/admin/structure/aiprompts/manage/{aiprompt_type}/delete",
 *     "edit-form" = "/admin/structure/aiprompts/manage/{aiprompt_type}",
 *     "collection" = "/admin/structure/aiprompts/types",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   }
 * )
 */
class AIPromptType extends ConfigEntityBundleBase implements AIPromptTypeInterface {

  /**
   * The aiprompt type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The aiprompt type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The description of the aiprompt type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function getDescription(): ?string {
    return $this->description;
  }
}