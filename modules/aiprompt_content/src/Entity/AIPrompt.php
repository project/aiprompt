<?php

namespace Drupal\aiprompt_content\Entity;

use Drupal\aiprompt\Traits\AIPromptCommons;
use Drupal\aiprompt_content\AIPromptContentInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EditorialContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * AI prompt entity.
 *
 * @ingroup aiprompt
 *
 * @ContentEntityType(
 *   id = "aiprompt",
 *   label = @Translation("AI prompt"),
 *   handlers = {
 *     "list_builder" = "Drupal\aiprompt_content\AIPromptListBuilder",
 *     "views_data" = "Drupal\aiprompt_content\Entity\AIPromptViewsData",
 *     "storage_schema" = "Drupal\aiprompt_content\AIPromptStorageSchema",
 *     "access" = "Drupal\aiprompt_content\AIPromptAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\aiprompt_content\Form\AIPromptForm",
 *       "add" = "Drupal\aiprompt_content\Form\AIPromptForm",
 *       "edit" = "Drupal\aiprompt_content\Form\AIPromptForm",
 *       "delete" = "Drupal\aiprompt_content\Form\AIPromptDeleteForm",
 *     },
 *   },
 *   base_table = "aiprompt",
 *   data_table = "aiprompt_field_data",
 *   revision_table = "aiprompt__revision",
 *   revision_data_table = "aiprompt_field_revision",
 *   show_revision_ui = TRUE, 
 *   admin_permission = "administer aiprompt content",
 *   translatable = TRUE, 
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *     "owner" = "uid",
 *     "revision" = "vid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   }, 
 *   links = {
 *     "add-page" = "/aiprompt/add",
 *     "add-form" = "/aiprompt/add/{type}",
 *     "edit-form" = "/aiprompt/{aiprompt}/edit",
 *     "delete-form" = "/aiprompt/{aiprompt}/delete",
 *     "collection" = "/admin/content/aiprompt"
 *   },
 *   bundle_entity_type = "aiprompt_type",
 *   field_ui_base_route = "entity.aiprompt_type.edit_form", 
 * )
 */
class AIPrompt extends EditorialContentEntityBase implements EntityOwnerInterface, AIPromptContentInterface  {

  use EntityOwnerTrait;
  use AIPromptCommons;

  /**
   * The AI Prompt segments.
   *
   * @var array
   */
  protected $segment_objects = [];

  /**
   * The AI Prompt arguments.
   *
   * @var array
   */
  protected $argument_objects = [];

  /**
   * For tracking the state of the object.
   *
   * @var array
   */
  protected $object_state;

  /**
   * The AI prompt segment plugin manager.
   *
   * @var \Drupal\aiprompt\AIPromptSegmentPluginManager
   */
  protected $aipromptSegmentPluginManager;

  /**
   * The AI prompt argument plugin manager.
   *
   * @var \Drupal\aiprompt\AIPromptArgumentPluginManager
   */
  protected $aipromptArgumentPluginManager;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the entity owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }

    $this->saveData();
  }

  /**
   * {@inheritdoc}
   */
  public function preSaveRevision(EntityStorageInterface $storage, \stdClass $record) {
    parent::preSaveRevision($storage, $record);

    if (!$this->isNewRevision() && isset($this->original) && (!isset($record->revision_log) || $record->revision_log === '')) {
      // If we are updating an existing block_content without adding a new
      // revision and the user did not supply a revision log, keep the existing
      // one.
      $record->revision_log = $this->original->getRevisionLogMessage();
    }
  }   
  

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    if(empty($entity_type)){
      return $fields;
    }
    $fields += static::ownerBaseFieldDefinitions($entity_type);

    $owner_key = $entity_type->getKey('owner');
    $fields[$owner_key]
      ->setLabel(t('Authored by'))
      ->setDescription(t('The username of the prompt author.'))
      ->setRevisionable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    $published_key = $entity_type->getKey('published');
    $fields[$published_key]
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => TRUE,
        ],
        'weight' => 120,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the entity was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'region' => 'hidden'
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the content was last edited.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription('')
      ->setDefaultValue('')
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'text_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['notes'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Administrative notes'))
      ->setDescription(t('Notes about this prompt.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(FALSE)
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -4
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['data'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Data'))
      ->setDescription(t(''))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setRequired(FALSE)
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'aiprompt_data_view_widget',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    parent::postLoad($storage, $entities);

    foreach ($entities as $entity) {
      $entity->data_cache = json_decode($entity->data->value ?? [], TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDataNestedValue(mixed $key): mixed {
    return NestedArray::getValue($this->data_cache, (array) $key);
  }

  /**
   * {@inheritdoc}
   */
  public function setDataNestedValue(mixed $key, mixed $value ): void {
    NestedArray::setValue($this->data_cache, (array) $key, $value, TRUE);

    if ($value === NULL) {
      NestedArray::unsetValue($this->data_cache, $key);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function saveData(): void {
    $this->set('data', json_encode($this->data_cache ?? [], JSON_PRETTY_PRINT));
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentsData(): array {
    return $this->getDataNestedValue('segments') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setSegmentsData(array $segments): void {
    $this->setDataNestedValue('segments', $segments);
  }

  /**
   * {@inheritdoc}
   */
  public function setSegmentData(array $segment, $machine_name = NULL): void {
    if (empty($machine_name)) {
      $machine_name = $this->generateSegmentMachineName();
    }
    $this->setDataNestedValue(['segments', $machine_name], $segment);
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentData($machine_name = NULL): array {
    $segment = !empty($machine_name) ? $this->getDataNestedValue(['segments', $machine_name]) : [];
    $segment['settings']['aiprompt_id'] = $this->id();
    return $segment;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSegmentData(string $machine_name): void {
    $this->setDataNestedValue(['segments', $machine_name], NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getArgumentsData(): array {
    return $this->getDataNestedValue('arguments') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getArgumentData(string $name = NULL): array {
    return !empty($name) ? $this->getDataNestedValue(['arguments', $name]) : [];
  }

  /**
   * {@inheritdoc}
   */
  public function updateArgumentData(array $data, string $name): void {
    $this->setDataNestedValue(['arguments', $name], $data);
  }

  /**
   * {@inheritdoc}
   */
  public function deleteArgumentData(string $name): void {
    $this->setDataNestedValue(['arguments', $name], NULL);
  }

}
