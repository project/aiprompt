<?php

namespace Drupal\aiprompt_content\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides the views data for the AI prompt entity type.
 */
class AIPromptViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['aiprompt']['preview'] = [
      'title' => $this->t('AI Prompt Preview'),
      'field' => [
        'title' => $this->t('AI Prompt Preview'),
        'help' => $this->t('Provides a preview of AI Prompt.'),
        'id' => 'aiprompt_preview',
      ],
    ];

    return $data;
  }

}