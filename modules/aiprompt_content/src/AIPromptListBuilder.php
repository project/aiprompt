<?php

namespace Drupal\aiprompt_content;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of aiprompt entities.
 *
 * @see \Drupal\aiprompt_content\Entity\AIPrompt
 */
class AIPromptListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $label = !empty($entity->label()) ? $entity->label() : $entity->id();
    $row['label'] = new Link($label, Url::fromRoute("entity.aiprompt.edit_form", ["aiprompt" => $entity->id()]));
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);

    // removes destination parameter
    if (!empty($operations['edit'])) {
      $operations['edit']['url'] = $entity->toUrl('edit-form');
    }

    return $operations;
  }

}

