<?php

namespace Drupal\aiprompt_content;

use Drupal\aiprompt\NestedDataInterface;
use Drupal\aiprompt\AIPromptInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining aiprompt entities.
 *
 * @ingroup aiprompt
 */
interface AIPromptContentInterface extends ContentEntityInterface, NestedDataInterface, AIPromptInterface {

  /**
   * Saves data field to entity.
   */
  public function saveData(): void;
}
