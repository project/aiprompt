<?php

/**
 * Implements hook_entity_extra_field_info().
 * 
 * Add "Prompt segments form" field to "Manage form" page.
 */
function aiprompt_content_entity_extra_field_info() {
  $extra_fields = [];

  $aiPromptTypes = \Drupal::entityTypeManager()
    ->getStorage('aiprompt_type')
    ->loadMultiple();

  foreach ($aiPromptTypes as $aiPromptType) {
    $extra_fields['aiprompt'][$aiPromptType->id()]['form']['segments_form'] = [
      'label' => t('Prompt segments form'),
      'description' => t('Segments form.'),
      'weight' => 0,
      'visible' => true,
    ];
  }

  return $extra_fields;
}

/**
 * Implements hook_menu_links_discovered_alter().
 * 
 * Create a menu link for each aiprompt type.
 */
function aiprompt_content_menu_links_discovered_alter(&$links) {
  $types = \Drupal::entityTypeManager()->getStorage('aiprompt_type')->loadMultiple();

  foreach ($types as $type) {
    $id = 'aiprompt.add.' . $type->id();

    $links[$id] = [
      'title' => t('Add @label', ['@label' => $type->label()]),
      'route_name' => 'aiprompt_content.aiprompt_add_form',
      'route_parameters' => ['aiprompt_type' => $type->id()],
      'parent' => 'entity.aiprompt.collection',
      'provider' => 'aiprompt',
    ];
  }
}
