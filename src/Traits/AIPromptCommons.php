<?php

namespace Drupal\aiprompt\Traits;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Uuid\Uuid;

/**
 * Common methods for AIPrompt entities.
 */
trait AIPromptCommons {

  /**
   * {@inheritdoc}
   */
  public function toString(): string {
    $output = [];

    foreach ($this->getEnabledSegmentsSortedByWeight() as $segment) {
      $output[] = $segment->toString();
    }
    $separator = PHP_EOL . PHP_EOL;
    return implode($separator, $output);
  }

  /**
   * {@inheritdoc}
   */
  public function buildPreview(): array {
    $build = [];

    foreach ($this->getEnabledSegmentsSortedByWeight() as $segment) {
      $build[] = $segment->buildPreview();
      $build[]['#markup'] = '</br>';
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function updateSegmentsFromSubmittedData($submitted_data): void {
    $segments = $this->getSegmentsData();

    foreach ($submitted_data as $machine_name => $vals) {
      if (isset($vals['status']['status_value'])) {
        $segments[$machine_name]['status'] = $vals['status']['status_value'];
      }

      if (!empty($vals['weight'])) {
        $segments[$machine_name]['weight'] = $vals['weight'];
      }

      if (!empty($vals['plugin']['dependency_data'])) {
        $segments[$machine_name]['dependency_data'] = json_decode($vals['plugin']['dependency_data'], TRUE);
      }
    }

    $segments = $this->migrateData($segments);
    $this->setSegmentsData($segments);
  }

  public function migrateData($segments_data) {

    $keys = array_keys($segments_data);
    $valid = FALSE;
    foreach ($keys as $key) {
      if (!Uuid::isValid($key) || empty($segments_data[$key])) {
        $valid = FALSE;
        break;
      }
    }

    if ($valid) return $segments_data;

    $uuid_service = \Drupal::service('uuid');

    $new_data = [];
    foreach ($segments_data as $key => $segment_data) {

      if (empty($segment_data)) {
        \Drupal::logger('aiprompt')->info(
          "Segment key '@key' deleted.",
          ['@key' => $key]
        );
        continue;
      }

      $new_key = $key;
      if (!Uuid::isValid($key)) {
        $new_key = $uuid_service->generate();
        \Drupal::logger('aiprompt')->info(
          "Segment key '@key' was was migrated to new key '@new_key'.",
          ['@key' => $key, '@new_key' => $new_key]
        );
      }
      
      elseif (empty($segment_data['plugin_id'])) {
        $parent_key = NULL;
        $parent = $segment_data["dependency_data"]["parent"] ?? NULL;
        if (!empty($parent)) {
          $parent_key = key($parent);
        }
        if (!empty($parent_key)) {
          if (!Uuid::isValid($parent_key)) {
            $parent_key = NULL;
          }
        }
        if (empty($parent_key)) {
          \Drupal::logger('aiprompt')->info(
            "Segment '@key' deleted.",
            ['@key' => $key]
          );
          continue;
        }
      }

      $new_data[$new_key] = $segment_data;
    }

    return $new_data;
  }

  /**
   * {@inheritdoc}
   */
  public function setArgumentValue(string $name, mixed $value): void {
    $this->loadArguments();
    if (!empty($this->argument_objects[$name])) {
      $this->argument_objects[$name]->setValue($value);
    } else {
      throw new \InvalidArgumentException(t('The argument key "@name" does not exist and must be configured in the UI before setting it.', ['@name' => $name]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getArgumentValue(string $key): mixed {
    $this->loadArguments();
    if (!empty($this->argument_objects[$key])) {
      return $this->argument_objects[$key]->getValue();
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getArguments(): array {
    $this->loadArguments();
    return $this->argument_objects ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument(?string $name, ?string $plugin_id = NULL): mixed {
    $this->loadArguments();
    if (!empty($name) && !empty($this->argument_objects[$name])) {
      return $this->argument_objects[$name];
    }
    if (!empty($plugin_id)) {
      return $this->createArgument($plugin_id);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteArgument(string $name): void {
    $this->deleteArgumentData($name);
  }

  /**
   * Helper to load argument objects.
   *
   * @param bool $force
   *   Option to reload arguments even if they are already loaded.
   */
  protected function loadArguments($force = FALSE) {
    if ($this->getObjectState('arguments-loaded') && !$force) {
      return;
    }
    $this->setObjectState('arguments-loaded', TRUE);

    $arguments_data = $this->getArgumentsData();
    foreach ($arguments_data as $name => $argument_data) {
      $argument = $this->loadArgument($argument_data['type'], $argument_data);
      $argument->setEntity($this);
      $this->argument_objects[$name] = $argument;
    }

    // Load extra arguments as part of inheritance feature.
    $segments = $this->getSegments();
    foreach ($segments as $name => $segment) {
      if ($segment->isEnabled() && $segment->hasExtraArguments()) {
        $extraArguments = $segment->getExtraArguments();
        foreach ($extraArguments as $name2 => $extraArgument) {
          if (isset($this->argument_objects[$name2])) continue;
          $this->argument_objects[$name2] = $extraArgument;
          $this->argument_objects[$name2]->setEntity($this);

          if (!$extraArgument->hasDependency('parent')) {
            $extraArgument->setDependency('parent', $name, $segment);
          }
        }
      }
    }
  }

  /**
   * Helper to load or create individual segment object.
   */
  protected function loadArgument(string $type, array $argument_data) {
    $plugin_manager = $this->getAIPromptArgumentPluginManager();
    return $plugin_manager->createInstance($type, $argument_data);
  }

  /**
   * Helper to load or create individual segment object.
   */
  protected function createArgument(string $type) {
    $argument = $this->loadArgument($type, $this->getArgumentData());
    $argument->setNew();
    $argument->setEntity($this);
    return $argument;
  }

  /**
   * {@inheritdoc}
   */
  public function getSegments(): array {
    $this->loadSegments();
    return $this->segment_objects ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSegment(?string $machine_name, ?string $plugin_id = NULL): mixed {
    $this->loadSegments();
    if (!empty($machine_name) && !empty($this->segment_objects[$machine_name])) {
      return $this->segment_objects[$machine_name];
    }
    if (!empty($plugin_id)) {
      return $this->createSegment($plugin_id);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteSegment(string $machine_name): void {
    $this->deleteSegmentData($machine_name);
  }

  /**
   * {@inheritdoc}
   */
  public function cloneSegment(string $machine_name): void {
    $segment = $this->getSegment($machine_name);
    $data = $segment->getData();
    NestedArray::unsetValue($data, ['dependency_data', 'parent']);

    // save data as new segment
    $this->setSegmentData($data);
  }

  /**
   * Helper to load segment objects.
   *
   * @param bool $force
   *   Option to reload segments even if they are already loaded.
   */
  protected function loadSegments($force = FALSE): void {
    if ($this->getObjectState('segments-loaded') && !$force) {
      return;
    }
    $this->setObjectState('segments-loaded', TRUE);

    $segments_data = $this->getSegmentsData();

    foreach ($segments_data as $uuid => $segment_data) {

      // Inherited segments can have some limited data saved about them,
      // so skip them if they are not overriden and so don't have all the data.
      if (empty($segment_data['plugin_id'])) {
        continue;
      }

      $segment = $this->loadSegment($segment_data['plugin_id'], $segment_data);
      $segment->setEntity($this);
      $segment->setName($uuid);

      $this->segment_objects[$uuid] = $segment;

      // Load extra segments as part of inheritance feature.
      if ($segment->isEnabled() && $segment->hasExtraSegments()) {
        $extraSegments = $segment->getExtraSegments();
        foreach ($extraSegments as $uuid2 => $extraSegment) {
          if (!$extraSegment->hasDependency('parent')) {
            $extraSegment->setDependency('parent', $uuid);
          }
          $this->segment_objects[$uuid2] = $extraSegment;
          $this->segment_objects[$uuid2]->setEntity($this);
        }
      }
    }

    // If segment was loaded as extra, it can have limited additional data, so process it.
    foreach ($segments_data as $machine_name => $segment_data) {
      if (empty($segment_data['plugin_id']) && !empty($this->segment_objects[$machine_name])) {
        $keys = ['status', 'weight'];
        foreach ($keys as $key) {
          if (isset($segment_data[$key])) {
            $this->segment_objects[$machine_name]->set($key, $segment_data[$key]);
          }
        }
      }
    }
  }

  /**
   * Helper to load or create individual segment object.
   */
  protected function loadSegment(string $plugin_id, array $segment_data) {
    $plugin_manager = $this->getAIPromptSegmentPluginManager();
    return $plugin_manager->createInstance($plugin_id, $segment_data);
  }

  /**
   * Helper to load or create individual segment object.
   */
  protected function createSegment(string $plugin_id) {
    $segment = $this->loadSegment($plugin_id, $this->getSegmentData());
    $segment->setNew();
    $segment->setEntity($this);
    return $segment;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledSegmentsSortedByWeight(): array {
    $segments = $this->getEnabledSegments();
    $this->sortSegmentsByWeight($segments);
    return $segments;
  }

  /**
   * {@inheritdoc}
   */
  public function getEnabledSegments(): array {
    $segments = $this->getSegments();

    $segments = array_filter($segments, function ($segment) {
      return $segment->isEnabled();
    });

    return $segments;
  }

  /**
   * {@inheritdoc}
   */
  public function getSegmentsSortedByWeight(): array {
    $segments = $this->getSegments();
    $this->sortSegmentsByWeight($segments);
    return $segments;
  }

  /**
   * Helper function to sort segments.
   */
  protected function sortSegmentsByWeight(array &$segments): void {
    uasort($segments, function($a, $b) {
      return $a->getWeight() - $b->getWeight();
    });
  }

  /**
   * Helper function to generate segment machine name.
   */
  protected function generateSegmentMachineName(): string {
    return \Drupal::service('uuid')->generate();
  }

  /**
   * Helper to get AI prompt plugin manager service.
   *
   * @return \Drupal\aiprompt\AIPromptSegmentPluginManager
   *   The AI prompt plugin manager service.
   */
  protected function getAIPromptSegmentPluginManager() {
    if (!$this->aipromptSegmentPluginManager) {
      $this->aipromptSegmentPluginManager = \Drupal::service('plugin.manager.aiprompt');
    }

    return $this->aipromptSegmentPluginManager;
  }

  /**
   * Helper to get AI prompt argument plugin manager service.
   *
   * @return \Drupal\aiprompt\AIPromptArgumentPluginManager
   *   The AI prompt plugin manager service.
   */
  protected function getAIPromptArgumentPluginManager() {
    if (!$this->aipromptArgumentPluginManager) {
      $this->aipromptArgumentPluginManager = \Drupal::service('plugin.manager.aiprompt_argument');
    }

    return $this->aipromptArgumentPluginManager;
  }

  /**
   * Helper to get AI prompt object state.
   */
  protected function getObjectState(string $key): mixed {
    return $this->object_state[$key] ?? NULL;
  }

  /**
   * Helper to set AI prompt object state.
   */
  protected function setObjectState(string $key, mixed $value): void {
    $this->object_state[$key] = $value;
  }

}
