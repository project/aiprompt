<?php

namespace Drupal\aiprompt\Traits;

use Drupal\aiprompt\Traits\AIPromptCommons;
use Drupal\aiprompt\Utility\AIPromptStringHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;

/**
 * Common form methods for prompt engineering.
 */
trait AIPromptCommonEngineeringForm {

  use AIPromptCommons;

  /**
   * Build segments form.
   */
  public function buildSegmentsForm(array $form, FormStateInterface $form_state) {
    $this->entity_type = $this->entity->getEntityTypeId();

    $element = [
      '#prefix' => '<div id="segments-form-ajax-wrapper">',
      '#suffix' => '</div>',
      '#attached' => ['library' => ['aiprompt/engineeringForm']],
      '#weight' => 1,
    ];

    $element['add_segment_wrapper'] = [
      '#type' => 'fieldset'
    ];
    $element['add_segment_wrapper']['segment_plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Add new prompt segment'),
      '#options' => array_map(function ($definition) {
         return $definition['label'];
       }, $this->aipromptSegmentPluginManager->getDefinitions()),
      '#empty_option' => $this->t('- Select a segment type -')
    ];
    $element['add_segment_wrapper']['actions']['add_segment'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add segment'),
      '#submit' => [[$this, 'addNewSegmentSubmit']],
      '#validate' => [[$this, 'addNewSegmentValidate']]
    ];
    $element['segments_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'segments-wrapper',
      ],
    ];
    $element['segments_wrapper']['segments_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Segment type'),
        $this->t('Preview'),
        $this->t('Status'),
        $this->t('Operations'),
        $this->t('Weight'),
      ],
      '#empty' => $this->t('No plugins added.'),
      '#attributes' => [
        'id' => 'aiprompt-segments-table',
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'aiprompt-segments-table-row-weight',
        ],
      ],
    ];

    $segments = $this->entity->getSegmentsSortedByWeight();
    //ksm($this->entity->getSegmentsData(), $segments);
    foreach ($segments as $segment_id => $segment) {
      $item = [];
      $row_class = [];

      if (!$segment->isEnabled()) {
        $row_class[] = 'disabled-segment';
      }

      if ($segment->hasExtraSegments()) {
        $row_class[] = 'has-extra-segments';
      }

      if ($segment->isDraggable()) {
        $item['#weight'] = $segment->getWeight();
        $row_class[] = 'draggable';
      }

      $item['#attributes'] = ['class' => $row_class];

      $item['plugin'] = [
        '#type' => 'container'
      ];
      $item['plugin']['plugin_type'] = [
        '#type' => 'markup',
        '#markup' => $segment->getPluginLabel(),
        '#suffix' => '</br>'
      ];
      $extraNotes = $segment->getExtraNotes();
      if (!empty($extraNotes)) {
        $item['plugin']['extra_notes'] = [
          '#type' => 'markup',
          '#markup' => $extraNotes,
          '#suffix' => '</br>'
        ];
      }
      $text = nl2br($segment->getConfigurationValue('notes'));
      $item['plugin']['admin_notes'] = [
        '#type' => 'markup',
        '#markup' => $text,
        '#allowed_tags' => ['br'],
      ];
      if ($segment->hasDependencies()) {
        $item['plugin']['dependency_data'] = [
          '#type' => 'hidden',
          '#value' => json_encode($segment->getDependencies())
        ];
      }

      $item['preview'] = $segment->buildPreview();
      $item['status'] = $this->buildStatus($segment);
      $item['operations'] = $this->buildOperations($segment, $segment_id);

      $item['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight for @title', ['@title' => $segment->getPluginLabel()]),
        '#title_display' => 'invisible',
        '#default_value' => $segment->getWeight(),
        '#attributes' => ['class' => ['aiprompt-segments-table-row-weight']],
      ];
      $element['segments_wrapper']['segments_table'][$segment_id] = $item;
    }
    $element['helper_actions'] = [
      '#type' => 'container',
      // '#type' => 'fieldset',
      // '#title' => 'Helper actions'
    ];
    $element['helper_actions']['display_output'] = [
      '#type' => 'submit',
      '#value' => $this->t('Display output'),
      '#submit' => [[$this, 'displayOutputSubmit']]
    ];

    $form['segments_form'] = $element;

    return $form;
  }

  /**
   * Build segment operations.
   */
  private function buildOperations($segment, $segment_id) {
    $operations = [];
    if ($segment->isActionAvailable('edit')) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute(
          "{$this->entity_type}.segment.edit_form",
          [$this->entity_type => $this->entity->id(), 'segment_id' => $segment_id]
        ),
      ];
    }
    if ($segment->isActionAvailable('status')) {
      // Status operation is done with jquery, hidden checkbox and main form submit.
      // @see https://www.drupal.org/project/aiprompt/issues/3436593
      $url = Url::fromUri('internal:#change-status');
      $operations['status'] = [
        'title' => $segment->isEnabled() ? $this->t('Disable') : $this->t('Enable'),
        'url' => $url,
        'attributes' => ['class' => ['segment-status-link']]
      ];
    }
    $confirm_operation_attributes = [
      'class' => [
        'use-ajax',
      ],
      'data-dialog-type' => 'modal',
      'data-dialog-options' => Json::encode([
        'width' => 700,
      ]),
    ];
    if ($segment->isActionAvailable('clone')) {
      $operations['clone'] = [
        'title' => $this->t('Clone'),
        'url' => Url::fromRoute(
          "{$this->entity_type}.segment.action_confirm_form", [
            $this->entity_type => $this->entity->id(),
            'segment_id' => $segment_id,
            'action' => 'clone'
          ]
        ),
        'attributes' => $confirm_operation_attributes,
      ];
    }
    if ($segment->isActionAvailable('restore')) {
      $operations['restore'] = [
        'title' => $this->t('Restore'),
        'url' => Url::fromRoute(
          "{$this->entity_type}.segment.action_confirm_form", [
            $this->entity_type => $this->entity->id(),
            'segment_id' => $segment_id,
            'action' => 'restore'
          ]
        ),
        'attributes' => $confirm_operation_attributes,
      ];
    }
    if ($segment->isActionAvailable('delete')) {
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute(
          "{$this->entity_type}.segment.action_confirm_form", [
            $this->entity_type => $this->entity->id(),
            'segment_id' => $segment_id,
            'action' => 'delete'
          ]
        ),
        'attributes' => $confirm_operation_attributes,
      ];
    }
    return [
      '#type' => 'operations',
      '#links' => $operations
    ];
  }

  /**
   * Build segment override status.
   */
  private function buildStatus($segment) {
    $build = [
      '#type' => 'container',
    ];
    $build['status_value'] = [
      '#type' => 'checkbox',
      '#default_value' => $segment->isEnabled(),
      '#attributes' => ['class' => ['segment-status-checkbox'], 'style' => 'display: none']
    ];

    $status = $segment->isEnabled() ? $this->t('✔') : $this->t('✘');
    if ($segment->hasParent()) {
      $status = $segment->isEnabled() ? $this->t('Enabled') : $this->t('Disabled');
    }
    $build['markup'] = [
      '#type' => 'markup',
      '#markup' => $status,
      '#weight' => 2,
    ];

    if (!$segment->hasParent()) {
      return $build;
    }

    $diff = $segment->checkOverrideDifference();

    $diff_status = $this->t('Default');
    $diff_class = ['segment-original-status', 'segment-original-status-default'];

    if (!empty($diff['orphan'])) {
      $diff_status = $this->t('Orphan');
      $diff_class = ['segment-original-status', 'segment-original-status-orphan'];
    }
    if (!empty($diff['overriden'])) {
      $diff_status = $this->t('Overriden');
      $diff_class = ['segment-original-status', 'segment-original-status-overriden'];
    }

    $diff_class = implode(' ', $diff_class);
    $markup = "<div class=\"{$diff_class}\">$diff_status</div>";

    $build['original']  = [
      '#type' => 'markup',
      '#markup' => $markup,
      '#weight' => 1,
    ];

    return $build;
  }

  /**
   * Validate segments form.
   */
  public function addNewSegmentValidate(array &$form, FormStateInterface $form_state) {
    $selectedPluginId = $form_state->getValue('segment_plugin');

    if (empty($selectedPluginId)) {
      $form_state->setErrorByName('segment_plugin', $this->t('Please select a plugin first.'));
    }
  }

  /**
   * Submit callback to add new segment
   */
  public function addNewSegmentSubmit(array $form, FormStateInterface $form_state): void {
    $selectedPluginId = $form_state->getValue('segment_plugin');

    $form_state->setRedirect("{$this->entity_type}.segment.add_form", [
      $this->entity_type => $this->entity->id(),
      'plugin_id' => $selectedPluginId,
    ]);
  }

  /**
   * Submit callback to display output.
   */
  public function displayOutputSubmit(array $form, FormStateInterface $form_state): void {

    $output = $this->entity->toString();

    $word_count = str_word_count($output);
    $token_count = AIPromptStringHelper::calcTokenCountBasic($output);
    $this->messenger()->addMessage($this->t(
      "Prompt has @word_count words and approximately @token_count number of tokens.",
      ['@word_count' => $word_count, '@token_count' => $token_count]
    ));

    if (class_exists('Kint') && function_exists('ksm') &&
        $this->currentUser()->hasPermission('access devel information')) {
      ksm($output);
    }
    else {
      $this->messenger()->addMessage(
        AIPromptStringHelper::prepareStringForStatusMessage($output)
      );
    }
  }

  /**
   * Build the arguments form.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildArgumentsForm(array $form, FormStateInterface $form_state) {

    $element = [
      '#prefix' => '<div id="arguments-form-ajax-wrapper">',
      '#suffix' => '</div>',
      '#type' => 'fieldset',
      '#title' => $this->t('Arguments'),
      '#weight' => 2,
    ];

    $element['arguments_table'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Type'),
        $this->t('Default Value'),
        $this->t('Description'),
        $this->t('Operations'),
      ],
      '#empty' => $this->t('No arguments defined.'),
    ];

    $arguments = $this->entity->getArguments();

    foreach ($arguments as $name => $argument) {
      $row = [];
      $row['name']['name'] = ['#plain_text' => $name];
      $extraNotes = $argument->getExtraNotes();
      if (!empty($extraNotes)) {
        $row['name']['extra_notes'] = [
          '#type' => 'markup',
          '#markup' => '</br>'.$extraNotes,
          '#suffix' => '</br>'
        ];
      }

      $row['type'] = ['#plain_text' => $argument->getType()];
      $row['default_value'] = ['#plain_text' => $argument->previewValue()];
      $row['description'] = ['#plain_text' => $argument->getDescription()];

      $operations = [];
      if ($argument->isActionAvailable('edit')) {
        $operations['edit'] = [
          'title' => $this->t('Edit'),
          'url' => Url::fromRoute("{$this->entity_type}.argument.edit_form", [
            $this->entity_type => $this->entity->id(),
            'argument_name' => $name,
          ]),
        ];
      }
      if ($argument->isActionAvailable('delete')) {
        $operations['delete'] = [
          'title' => $this->t('Delete'),
          'url' => Url::fromRoute(
            "{$this->entity_type}.argument.delete_form",
            [$this->entity_type => $this->entity->id(), 'argument_name' => $name]
          ),
          'attributes' => [
            'class' => [
              'use-ajax',
            ],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode([
              'width' => 700,
            ]),
          ],
        ];
      }

      $row['operations'] = [
        '#type' => 'operations',
        '#links' => $operations
      ];

      $element['arguments_table'][$name] = $row;
    }

    $element['add_argument_wrapper'] = [
      '#type' => 'container',
    ];

    $element['add_argument_wrapper']['argument_plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Add new argument'),
      '#options' => array_map(function ($definition) {
        return $definition['label'];
      }, $this->aipromptArgumentPluginManager->getDefinitions()),
      '#empty_option' => $this->t('- Select an argument type -')
    ];
    $element['add_argument_wrapper']['add_argument'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add argument'),
      '#submit' => [[$this, 'addNewArgumentSubmit']],
      '#validate' => [[$this, 'addNewArgumentValidate']]
    ];

    $form['arguments_form'] = $element;

    return $form;
  }

  /**
   * Validate arguments form.
   */
  public function addNewArgumentValidate(array &$form, FormStateInterface $form_state) {
    $selectedPluginId = $form_state->getValue('argument_plugin');
    if (empty($selectedPluginId)) {
      $form_state->setErrorByName('argument_plugin', $this->t('Please select a plugin first.'));
    }
  }

  /**
   * Submit callback to add new argument.
   */
  public function addNewArgumentSubmit(array $form, FormStateInterface $form_state): void {
    $selectedPluginId = $form_state->getValue('argument_plugin');

    $form_state->setRedirect("{$this->entity_type}.argument.add_form", [
      $this->entity_type => $this->entity->id(),
      'plugin_id' => $selectedPluginId,
    ]);
  }

}
