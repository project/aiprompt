<?php

namespace Drupal\aiprompt\Utility;

use Drupal\Component\Utility\Html;
use Drupal\Core\Render\Markup;

/**
 * Provides helper to operate on strings.
 */
class AIPromptStringHelper {

  /**
   * Converts html string to plain text while preserving new lines (which were
   * defined in html)
   *
   * @param string $string
   *   String containing html
   *
   * @return string
   *   plain text with new lines preserved
   */
  public static function htmlToPlainText(string $string): string {
    $block_tags = '/<(p|div|h3|h4|h5|h6|cite|blockquote|code|li|tr|td|th|br)[^>]*>/i';
    $string = preg_replace($block_tags, "\n", $string);
    $string = strip_tags($string);
    // The flags ENT_QUOTES and ENT_HTML5 tell to decode both double and single quotes.
    $string = html_entity_decode($string, ENT_QUOTES | ENT_HTML5, 'UTF-8');
    $string = trim($string);
    return $string;
  }

  /**
   * Prepare status message for the user.
   * Escape HTML and preserve new lines.
   */
  public static function prepareStringForStatusMessage(string $string): Markup {
    $string = Html::escape($string);
    return Markup::create(nl2br($string));
  }

  /**
   * Most basic token count calculation using simple math.
   *
   * @param string $string
   *   Text to calculate tokens for
   *
   * @return integer
   *   Token count
   */
  public static function calcTokenCountBasic(string $string): int {
    $word_count = str_word_count($string);
    return (int) round($word_count * 2); // text-token ratio can vary widely depending string is text or code
  }

}
