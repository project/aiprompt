<?php

namespace Drupal\aiprompt\Utility;

/**
 * Provides helper to operate on strings.
 */
class AIPromptArrayHelper {

  /**
   * Helper method to remove not needed values from deep array which starts
   * with underscore, for example '_operations'
   */
  public static function removeKeysStartingWithUnderscore($values) {
    foreach ($values as $key => $value) {
      if (strpos($key, '_') === 0) {
        unset($values[$key]);
      }
      elseif (is_array($value)) {
        $values[$key] = self::removeKeysStartingWithUnderscore($value);
      }
    }
    return $values;
  }

  /**
   * Extracts a column of values from an array while preserving original keys.
   *
   * @param array $array
   *   The input array.
   * @param string $key
   *   The key of the column to retrieve.
   *
   * @return array
   *   The array of column values with original keys.
   */
  public static function getArrayColumn(array &$array, string $key): array {
    return array_combine(array_keys($array), array_column($array, $key));
  }

}
