<?php

namespace Drupal\aiprompt\Plugin\AIPromptSegment;

use Drupal\aiprompt\Annotation\AIPromptSegment;
use Drupal\aiprompt\Plugin\AIPromptSegmentBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Text' AI Prompt plugin.
 *
 * @AIPromptSegment(
 *   id = "text",
 *   label = @Translation("Text"),
 *   description = @Translation("AI Prompt text segment")
 * )
 */
class Text extends AIPromptSegmentBase {

  /**
   * {@inheritdoc}
   */
  public function defineConfiguration(): array {
    return [
      'text' => ['default' => '']
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(): string {
    return $this->getPluginConfigValueTokenized('text');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text'),
      '#default_value' => $this->getPluginConfigValue('text'),
      '#required' => TRUE,
      '#description' => $this->getAvailableTokensTreeBuildElement(),
    ];

    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

}
