<?php

namespace Drupal\aiprompt\Plugin\AIPromptSegment;

use Drupal\aiprompt\Plugin\AIPromptSegmentBase;
use Drupal\Component\Utility\PlainTextOutput;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\filter\Entity\FilterFormat;
use Drupal\views\Views as ViewsService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Views' AI Prompt plugin.
 *
 * @AIPromptSegment(
 *   id = "views",
 *   label = @Translation("Views"),
 *   description = @Translation("Embeds content listed by a specific view.")
 * )
 */
class Views extends AIPromptSegmentBase implements ContainerFactoryPluginInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructs a new ViewsPrompt instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              CurrentRouteMatch $currentRouteMatch,
                              EntityTypeManagerInterface $entity_type_manager,
                              $token_service,
                              AccountProxyInterface $current_user,
                              RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $currentRouteMatch,
                        $entity_type_manager, $token_service);
    $this->currentUser = $current_user;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('current_user'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defineConfiguration(): array {
    return [
      'view_id' => ['default' => ''],
      'display_id' => ['default' => ''],
      'arguments' => ['default' => ''],
      'parameters' => ['default' => ''],
      'text_format' => ['default' => 'plain_text']
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $options_view_id = $this->getViewsOptions();
    $value_view_id = $this->getValueViewId($form_state, $options_view_id);
    $form['view_id'] = [
      '#type' => 'select',
      '#title' => $this->t('View ID'),
      '#description' => $this->t('Select the view to embed.'),
      '#default_value' => $value_view_id,
      '#options' => $options_view_id,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxUpdateDisplay'],
        'wrapper' => 'edit-display-id-wrapper',
      ],
      '#attributes' => ['autocomplete' => 'off']
    ];

    $options_display_id = $this->getDisplayOptions($value_view_id);
    $value_display_id = $this->getValueDisplayId($form_state, $options_display_id);
    $form['display_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Display ID'),
      '#description' => $this->t('Select the display of the view.'),
      '#default_value' => $value_display_id,
      '#options' =>  $options_display_id,
      '#prefix' => '<div id="edit-display-id-wrapper">',
      '#suffix' => '</div>',
      '#required' => TRUE,
      '#attributes' => ['autocomplete' => 'off']
    ];

    $form['arguments'] = [
      '#type' => 'textarea',
      '#title' => $this->t('View contextual filters'),
      '#description' => $this->t('Enter each argument on a new line. For multiple
         values for a single filter, separate them with a "+" or "," depending on
         the view setup. For example:<br/>arg1<br/>arg2+arg3 (matches arg2 OR arg3)
         <br/>arg4,arg5 (matches arg4 AND arg5)<br/>
         <strong>This element also support tokens.</strong>'),
      '#rows' => 4,
      '#default_value' => $this->getPluginConfigValue('arguments'),
      '#placeholder' => $this->t("Example:\narg1\narg2+arg3\narg4,arg5"),
    ];
    $form['arguments_tokens'] = $this->getAvailableTokensTreeBuildElement();

    $form['parameters'] = [
      '#type' => 'textarea',
      '#title' => $this->t('View URL parameters'),
      '#description' => $this->t('Enter each parameter on a new line in the format "key=value".
         Parameters can include filtering criteria, sort options, items per page,
         and page number. You can often find these parameter names within the URL
         when viewing your view or views admin UI. Examples:<br/>
         field_example_value=some_value<br/>sort_by=title<br/>
         sort_order=DESC<br/>items_per_page=10<br/>page=2<br/>
         <strong>This element also support tokens.</strong>'),
      '#default_value' => $this->getPluginConfigValue('parameters'),
      '#placeholder' => "field_example_value=some_value\nsort_by=title\nsort_order=DESC\nitems_per_page=10\npage=2",
    ];
    $form['parameters_tokens'] = $this->getAvailableTokensTreeBuildElement();

    $text_format_options = $this->getTextFormatOptions();
    $form['text_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Text Format'),
      '#description' => $this->t('Text format will be used to process rendered view and prepare
        string output.<br/>Various filters are provided out-of-the-box to achieve best results,
        such as "AI Prompt engineering: HTML to Plain text" (or maybe "AI Prompt engineering: HTML to
        Markdown" at some point). You can enable these filters when creating or configuring
        text format.'),
      '#default_value' => $this->getPluginConfigValue('text_format'),
      '#options' => $text_format_options,
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::validateConfigurationForm($form, $form_state);

    // View selection validation
    $view_id = $form_state->getValue(['view_id']);
    $display_id = $form_state->getValue(['display_id']);
    $view = ViewsService::getView($view_id);

    if (empty($view)) {
      $form_state->setErrorByName('view_id',
        $this->t('The view with machine name "@view_id" does not exist.',
          ['@view_id' => $view_id])
      );
    }
    else {
      $view->initDisplay();
      if (empty($view->displayHandlers->get($display_id))) {
        $form_state->setErrorByName('display_id',
          $this->t('The display with ID "@display_id" does not exist in view "@view_id".',
            ['@display_id' => $display_id, '@view_id' => $view_id])
        );
      }
    }

    // Parameters validation
    $parameters = trim($form_state->getValue('parameters'));
    if (!empty($parameters)) {
      $parameters = explode("\n", $parameters);
      foreach ($parameters as $parameter) {
        $parts = explode('=', $parameter);
        if (count($parts) !== 2) {
          $form_state->setErrorByName('parameters',
            $this->t('Each parameter line must be in the format "key=value",
                      and both key and value must be provided.')
          );
          break;
        }
        $key = trim($parts[0]);
        $value = trim($parts[1]);
        if (empty($key) || empty($value)) {
          $form_state->setErrorByName('parameters',
            $this->t('Empty keys or values are not allowed. Make sure each parameter line is
                      in the format "key=value" with both a valid key and value.'));
        }
      }
    }
  }

  /**
   * AJAX callback to update the displays select list when a view is selected.
   */
  public function ajaxUpdateDisplay(array $form, FormStateInterface $form_state): array {
    return $form['display_id'];
  }

  /**
   * Helper function to get options for the views select list.
   */
  protected function getViewsOptions(): array {
    $options = [];
    foreach (ViewsService::getAllViews() as $view_id => $view) {
      /** @var \Drupal\views\Entity\View $view */
      $options[$view_id] = $view->label();
    }
    return $options;
  }

  /**
   * Helper function to get options for the display select list based on the selected view.
   */
  protected function getDisplayOptions(string $view_id): array {
    $options = [];
    $view = ViewsService::getView($view_id);
    if ($view) {
      foreach ($view->storage->get('display') as $display_id => $display) {
        $options[$display_id] = $display['display_title'];
      }
    }
    return $options;
  }

  /**
   * Helper function to determine default value of view_id form element.
   */
  protected function getValueViewId(FormStateInterface $form_state, array $options) {
    $value = $form_state->getValue('view_id');
    if (empty($value)) {
      $value = $this->getPluginConfigValue('view_id');
    }
    if (array_key_exists($value, $options)) {
      return $value;
    }
    return key($options);
  }

  /**
   * Helper function to determine default value of display_id form element.
   */
  protected function getValueDisplayId(FormStateInterface $form_state, array $options) {
    $value = $form_state->getValue('display_id');
    if (empty($value)) {
      $value = $this->getPluginConfigValue('display_id');
    }
    if (array_key_exists($value, $options)) {
      return $value;
    }
    return key($options);
  }

  /**
   * Helper function to get options for text formats select list.
   */
  protected function getTextFormatOptions(): array {
    $text_formats = filter_formats($this->currentUser);
    $options = [];
    foreach ($text_formats as $format) {
      $options[$format->id()] = $format->label();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render(): string {
    $view_id = $this->getPluginConfigValue('view_id');
    $display_id = $this->getPluginConfigValue('display_id');
    $arguments = $this->getPluginConfigValueTokenized('arguments');
    $parameters = $this->getPluginConfigValueTokenized('parameters');
    $text_format_id = $this->getPluginConfigValue('text_format');

    $view_output = '';
    $view = ViewsService::getView($view_id);

    if (is_object($view)) {
      $view->setDisplay($display_id);

      if (!empty($arguments)) {
        $view->setArguments(explode("\n", $arguments));
      }
      if (!empty($parameters)) {
        $params = [];
        $parameter_lines = explode("\n", $parameters);
        foreach ($parameter_lines as $parameter) {
          list($key, $value) = explode('=', $parameter, 2);
          $params[trim($key)] = trim($value);
        }
        $view->setExposedInput($params);
      }
      $view->display_handler->setOption('exposed_block', TRUE); // Hide exposed filters
      $view->execute();
      $rendered_view = $view->render();
      $view_output = $this->renderer->renderRoot($rendered_view);
    }

    // Process views output with text format
    if ($text_format_id && $view_output) {
      $filter_format = $this->entityTypeManager->getStorage('filter_format')->load($text_format_id);
      if ($filter_format) {
        $view_output = [
          '#type' => 'processed_text',
          '#text' => $view_output,
          '#format' => $text_format_id,
        ];
        $view_output = $this->renderer->renderRoot($view_output);
      }
    }
    return (string) $view_output;
  }
}
