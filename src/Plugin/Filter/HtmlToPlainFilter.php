<?php

namespace Drupal\aiprompt\Plugin\Filter;

use Drupal\aiprompt\Utility\AIPromptStringHelper;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * @Filter(
 *   id = "aiprompt_html_to_plain",
 *   title = @Translation("AI Prompt engineering: HTML to Plain text"),
 *   description = @Translation("Removes HTML tags while preserving new lines (which were defined in html)"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class HtmlToPlainFilter extends FilterBase {

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $result = new FilterProcessResult($text);
    $text = AIPromptStringHelper::htmlToPlainText($text);
    return $result->setProcessedText($text);
  }
}
