<?php

namespace Drupal\aiprompt\Plugin\AIPromptArgument;

use Drupal\aiprompt\Annotation\AIPromptArgument;
use Drupal\aiprompt\Plugin\AIPromptArgumentBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an 'Integer' Argument plugin.
 *
 * @AIPromptArgument(
 *   id = "integer",
 *   label = @Translation("integer"),
 *   description = @Translation("Integer prompt argument")
 * )
 */
class IntegerArgument extends AIPromptArgumentBase {

  /**
   * {@inheritdoc}
   */
  public function defineConfiguration(): array {
    return [
      'default_value' => ['default' => 0],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['default_value'] = [
      '#type' => 'number',
      '#title' => $this->t('Default Value'),
      '#default_value' => $this->getFormValue('default_value', $form_state),
      '#required' => TRUE,
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function isValueValid(mixed $value): bool {
    return is_numeric($value);
  }

}
