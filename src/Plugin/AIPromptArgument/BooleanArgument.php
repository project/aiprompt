<?php

namespace Drupal\aiprompt\Plugin\AIPromptArgument;

use Drupal\aiprompt\Annotation\AIPromptArgument;
use Drupal\aiprompt\Plugin\AIPromptArgumentBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'boolean' Argument plugin.
 *
 * @AIPromptArgument(
 *   id = "boolean",
 *   label = @Translation("boolean"),
 *   description = @Translation("Boolean prompt argument")
 * )
 */
class BooleanArgument extends AIPromptArgumentBase {

  /**
   * {@inheritdoc}
   */
  public function defineConfiguration(): array {
    return [
      'default_value' => ['default' => FALSE],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    unset($form['default_value']);
    $form['wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Default value'),
      '#weight' => 10,
    ];
    $form['wrapper']['default_value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('True'),
      '#default_value' => $this->getFormValue('default_value', $form_state),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function isValueValid(mixed $value): bool {
    return is_bool ($value) || in_array($value, [0, 1]);
  }
}
