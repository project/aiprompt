<?php

namespace Drupal\aiprompt\Plugin\AIPromptArgument;

use Drupal\aiprompt\Annotation\AIPromptArgument;
use Drupal\aiprompt\Plugin\AIPromptArgumentBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'String' Argument plugin.
 *
 * @AIPromptArgument(
 *   id = "string",
 *   label = @Translation("string"),
 *   description = @Translation("String prompt argument")
 * )
 */
class StringArgument extends AIPromptArgumentBase {

  /**
   * {@inheritdoc}
   */
  public function defineConfiguration(): array {
    return [
      'default_value' => ['default' => ''],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['default_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Value'),
      '#default_value' => $this->getFormValue('default_value', $form_state),
      '#required' => TRUE,
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function isValueValid(mixed $value): bool {
    return is_string($value);
  }

}
