<?php

namespace Drupal\aiprompt\Plugin\AIPromptArgument;

use Drupal\aiprompt\Annotation\AIPromptArgument;
use Drupal\aiprompt\Plugin\AIPromptArgumentBase;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides an Entity argument plugin.
 *
 * @AIPromptArgument(
 *   id = "entity",
 *   label = @Translation("entity"),
 *   description = @Translation("Entity prompt argument")
 * )
 */
class EntityArgument extends AIPromptArgumentBase {

  /**
   * {@inheritdoc}
   */
  public function defineConfiguration(): array {
    return [
      'entity_type' => ['default' => 'node'],
      'default_value' => ['default' => NULL], // entity id
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $entity_types = $this->entityTypeManager->getDefinitions();

    $entity_type_options = [];
    foreach ($entity_types as $entity_type_id => $entity_type) {
      $entity_type_options[$entity_type_id] = $entity_type->getLabel();
    }

    $entity_type = $this->getFormValue('entity_type', $form_state);
    $form['entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type'),
      '#description' => $this->t('Select the entity type.'),
      '#options' => $entity_type_options,
      '#default_value' => $entity_type,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxUpdateDefaultValueElement'],
        'wrapper' => 'default-value-wrapper',
      ],
      '#attributes' => ['autocomplete' => 'off'],
      '#weight' => 8,
    ];

    $value = $this->getFormValue('default_value', $form_state);
    $storage = $this->getStorage();

    $form['default_value'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Default @type entity', ['@type' => $entity_type]),
      '#target_type' => $entity_type,
      '#default_value' => $value ? $storage->load($value) : NULL,
      '#prefix' => '<div id="default-value-wrapper">',
      '#suffix' => '</div>',
      '#required' => TRUE,
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * AJAX callback to update the default value field based on the selected entity type.
   */
  public function ajaxUpdateDefaultValueElement(array &$form, FormStateInterface $form_state) {
    return $form['default_value'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function isValueValid(mixed $value): bool {
    if (is_object($value) && !empty($value->id()) && $value->getEntityTypeId() === $this->configuration['entity_type']) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function validateDefaultValue(mixed $value): bool {
    return is_numeric($value) || is_string($value);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(): mixed {
    if ($this->value) {
      return $this->value;
    }

    $storage = $this->getStorage();
    return $storage->load($this->configuration['default_value']);
  }

  private function getStorage($entity_type = NULL) {
    return $this->entityTypeManager->getStorage($entity_type ?? $this->getConfigurationValue('entity_type'));
  }

  public function previewValue(): string {
    $entity = $this->getValue();
    $label = $entity->label();
    $id = $entity->id();
    $type = $this->getConfigurationValue('entity_type');
    return "($type id: $id) $label";
  }

}
