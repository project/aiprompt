<?php

namespace Drupal\aiprompt\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines an interface for AI Prompt plugins.
 */
interface AIPromptSegmentInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Defines default base configuration of the plugin.
   *
   * @return array
   */
  public function baseDefaults(): array;

  /**
   * Defines default configuration of the plugin.
   * 
   * @return array
   */
  public function defineConfiguration(): array;

  /**
   * Gets default configuration of the plugin (both parent and children combined)
   * 
   * @return array
   */
  public function getDefaultConfiguration(): array;

  /**
   * Get plugin configuration.
   *
   * @return mixed
   *   The configuration.
   */
  public function getPluginConfig(): mixed;

  /**
   * Get plugin configuration specific value.
   *
   * @param int|string $key
   *   The key of the configuration.
   *
   * @return mixed
   *   The value of the configuration.
   */
  public function getPluginConfigValue(int|string $key): mixed;

  /**
   * Get configuration value (saved or default).
   * 
   * @param string|array $key
   *   If $fieldname is a string, it will return configuration value.
   *   If $fieldname is an array, each element of the array will be used as a nested key.
   *   If $fieldname = ['foo', 'bar'] it will return $configuration['foo']['bar'].
   *
   * @return mixed
   *   The value of the configuration.
   */
  public function &getConfigurationValue(mixed $key): mixed;

  /**
   * Get the base configuration keys.
   *
   * @return array
   *   The base configuration keys.
   */
  public function getBaseConfigKeys(): array;

  /**
   * Get the plugin configuration keys.
   *
   * @return array
   *   The plugin configuration keys.
   */
  public function getPluginConfigKeys(): array;

  /**
   * Returns output string of segment plugin.
   * Every plugin have to define this method.
   *
   * @return string
   */
  public function render(): string;

  /**
   * Get build array of segment preview
   *
   * @return array
   *   Build array.
   */
  public function buildPreview(): array;

  /**
   * Returns a form for the plugin configuration.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array;

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Returns the status of the segment.
   *
   * @return bool
   *   The status.
   */
  public function isEnabled(): bool;

  /**
   * Returns the weight of the segment.
   *
   * @return int
   *   The weight.
   */
  public function getWeight(): int;

  /**
   * Get tokenized plugin configuration.
   *
   * @return mixed
   *   The tokenized configuration.
   */
  public function getPluginConfigTokenized(): mixed;

  /**
   * Get single tokenized plugin configuration value.
   *
   * @param int|string $key
   *   The key of the configuration value.
   *
   * @return mixed
   *   The tokenized configuration value.
   */
  public function getPluginConfigValueTokenized(int|string $key): mixed;

  /**
   * Retrieves available token data for the current AI prompt entity.
   *
   * @return array
   *   Associative array of available tokens data.
   */
  public function getAvailableTokensData(): array;

  /**
   * Retrieves a render-able element to display available tokens tree.
   *
   * @return array
   *   Render-able array representing the token tree link.
   */
  public function getAvailableTokensTreeBuildElement(): array;

}
