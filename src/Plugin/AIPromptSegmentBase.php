<?php

namespace Drupal\aiprompt\Plugin;

use Drupal\aiprompt\Utility\AIPromptArrayHelper;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for AI Prompt plugins.
 */
abstract class AIPromptSegmentBase extends PluginBase implements AIPromptSegmentInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
   * The Current Route Match service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
  * The entity type manager.
  *
  * @var \Drupal\Core\Entity\EntityTypeManagerInterface
  */
  protected $entityTypeManager;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenService;

  /**
   * The prompt entity.
   */
  protected $entity;

  /**
   * Segment name.
   */
  protected string $name;

  /**
   * Segments status.
   */
  protected bool $status;

  /**
   * Segment weight.
   */
  protected int $weight;

  /**
   * Data about segment dependencies.
   */
  protected array $dependency_data;

  /**
   * Is segment new (not yet saved to the database)
   */
  protected bool $is_new;

  /**
   * Constructs a new ExamplePluginBase.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              CurrentRouteMatch $currentRouteMatch,
                              EntityTypeManagerInterface $entity_type_manager,
                              $token_service) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $currentRouteMatch;
    $this->entityTypeManager = $entity_type_manager;
    $this->tokenService = $token_service;
    $this->loadSegmentValues();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('token')
    );
  }

  public function setEntity($entity) {
    $this->entity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function baseDefaults(): array {
    return [
      'weight' => [
        'default' => 0,
      ],
      'status' => [
        'default' => TRUE
      ],
      'header' => [
        'default' => ''
      ],
      'header_empty_status' => [
        'default' => TRUE
      ],
      'notes' => [
        'default' => ''
      ],
      'dependency_data' => [
        'default' => []
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function defineConfiguration(): array;

  /**
   * {@inheritdoc}
   */
  public function getDefaultConfiguration(): array {
    $base_defaults = $this->baseDefaults();
    $plugin_defaults = $this->defineConfiguration();

    $defaults = AIPromptArrayHelper::getArrayColumn($base_defaults, 'default');
    $defaults['settings'] = AIPromptArrayHelper::getArrayColumn($plugin_defaults, 'default');
    return $defaults;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfig(): mixed {
    return $this->getConfigurationValue('settings');
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfigValue(int|string $key): mixed {
    return $this->getConfigurationValue(['settings', $key]);
  }

  /**
   * {@inheritdoc}
   */
  public function &getConfigurationValue(mixed $key): mixed {

    // get saved configuration
    $value =& NestedArray::getValue($this->configuration, (array) $key);

    // get default configuration
    if ($value === NULL) {
      $defaults = $this->getDefaultConfiguration();
      $value =& NestedArray::getValue($defaults, (array) $key);
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseConfigKeys(): array {
    $default = $this->baseDefaults();
    return array_keys($default);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfigKeys(): array {
    $default = $this->defineConfiguration();
    return array_keys($default);
  }

  /**
   * Return all segment data.
   */
  final public function getData(): array {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function render(): string;

  /**
   * Generate output string for preview
   *
   * @return string
   */
  protected function preview(): string {
    $string = $this->toString();

    $max_length = $this->isEnabled() ? 3000 : 1000;

    if (strlen($string) > $max_length) {
      $string = mb_substr($string, 0, $max_length) . '...';
    }

    return $string;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPreview(): array {
    $string = $this->preview();
    return [
      '#type' => 'markup',
      '#markup' => nl2br(Html::escape($string)),
      '#allowed_tags' => ['br']
    ];
  }

  /**
   * Returns output string of the segment.
   *
   * @return string
   */
  final public function toString(): string {
    $output = $this->render();
    $header = $this->getConfigurationValueTokenized('header');
    $header_empty_status = $this->getConfigurationValueTokenized('header_empty_status');

    $header_enabled = !empty($header);
    if (empty($output) && !$header_empty_status) {
      $header_enabled = FALSE;
    }

    $string = '';
    if ($header_enabled) {
      $string .= $header.PHP_EOL;
    }
    $string .= $output;
    return $string;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $form['extra'] = [
      '#type' => 'details',
      '#title' => $this->t('Extra settings'),
      '#weight' => 4
    ];
    $form['extra']['header'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Header text'),
      '#default_value' => $this->getConfigurationValue('header'),
      '#description' => $this->t('Add additional text before the segment output. Tokens can be used.'),
    ];
    $form['extra']['tokens_info'] = $this->getAvailableTokensTreeBuildElement();
    $form['extra']['header_empty_status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show header even if segment produce no output'),
      '#default_value' => $this->getConfigurationValue('header_empty_status'),
      '#description' => $this->t('Uncheck this box to hide header if segment result is empty.'),
    ];
    $form['extra']['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#default_value' => $this->getWeight(),
      '#description' => $this->t('When listing segments, those with smaller weights will be displayed first.'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->isEnabled(),
      '#description' => $this->t('Uncheck this box to disable this prompt segment.'),
      '#weight' => 5
    ];

    $form['notes'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Administrative notes'),
      '#default_value' => $this->getConfigurationValue('notes'),
      '#required' => FALSE,
      '#weight' => -5
    ];

    if ($this->hasDependencies()) {
      $form['dependency_data'] = [
        '#type' => 'hidden',
        '#value' => json_encode($this->getDependencies())
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(FormStateInterface $form_state) {
    $values = $this->getBaseFormValues($form_state);
    $values['settings'] = $this->getExtendedFormValues($form_state);
    $values['plugin_id'] = $this->getPluginId();
    $this->entity->setSegmentData($values, $this->getName());
    $this->entity->save();
  }

  /**
   * Get plugin label.
   */
  public function getPluginLabel() {
    return $this->getPluginDefinition()['label'];
  }

  /**
   * Set prompt segment name (for context how segment is called by parent entity).
   *
   * @param array $name
   *   Name of the segment as it already is within entity.
   */
  public function setName(string $name): void {
    $this->name = $name;
  }

  /**
   * Get prompt segment name.
   *
   * @return string|null
   *   Segment name or null if it is not set.
   */
  public function getName(): ?string {
    return $this->name ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraNotes(): string {
    $parents = $this->getDependencies('parent');
    $output = '';
    foreach ($parents as $segment_id => $data) {
      $parent_segment = $this->entity->getSegment($segment_id);
      $output .= $this->t('<em>(from @link)</em></br>', 
        ['@link' => $parent_segment->getPromptLink()->toString()]
      );
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function isNew(): bool {
    return $this->is_new ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setNew(): void {
    $this->is_new = TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isActionAvailable(string $action): bool {

    switch ($action) {

      case 'status':
        return TRUE;

      case 'edit':
        return TRUE;

      case 'clone':
        return TRUE;

      case 'delete':
        return !$this->hasParent();

      case 'restore':
        return $this->isOverriden();
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function isDraggable(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
   public function set($key, $val): void {
     $this->{$key} = $val;
  }

  /**
   * {@inheritdoc}
   */
  public function hasExtraSegments() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasExtraArguments() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function provideExtraSegments() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function provideExtraArguments() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraSegments() {
    return $this->provideExtraSegments();
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraArguments() {
    return $this->provideExtraArguments();
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies($type = NULL) {
    if (empty($type)) {
      return $this->dependency_data ?? [];
    }

    return $this->dependency_data[$type] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setDependency($type, $key, $data = []) {
    $this->dependency_data[$type][$key] = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function hasDependencies() {
    return !empty($this->dependency_data);
  }

  /**
   * {@inheritdoc}
   */
  public function hasDependency($type) {
    return !empty($this->dependency_data[$type]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencyId($type) {
    if ($this->hasDependency($type)) {
      return key($this->dependency_data[$type]);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function checkOverrideDifference() {
    $original = $this->getOriginal();
    if (empty($original)) {
      return ['orphan' => TRUE];
    }
    $current_settings = json_encode($this->getPluginConfig());
    $orig_settings = json_encode($original->getPluginConfig());
    if ($current_settings != $orig_settings) {
      //ksm($this->getName(), 'difference', $current_settings, $orig_settings);
      return ['overriden' => TRUE];
    }
    return FALSE;
  }

  public function isOverriden() {
    $diff = $this->checkOverrideDifference();
    if (!empty($diff['overriden'])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasParent() {
    return $this->hasDependency('parent');
  }

  /**
   * {@inheritdoc}
   */
  public function getParentId() {
    return $this->getDependencyId('parent');
  }

  /**
   * {@inheritdoc}
   */
  public function getParent() {
    $parent_id = $this->getParentId();
    return $this->entity->getSegment($parent_id);
  }

  /**
   * {@inheritdoc}
   */
  public function getOriginal() {

    if (! $this->hasParent()) {
      return NULL;
    }

    $parent_segment = $this->getParent();

    if (empty($parent_segment)) {
      ksm($this->getName(), 'no parent');
      return NULL;
    }

    $parent_prompt = $parent_segment->getPrompt();

    if (empty($parent_prompt)) {
      ksm($this->getName(), 'no parent prompt');
      return NULL;
    }

    $original_segment = $parent_prompt->getSegment($this->getName());

    if (empty($original_segment)) {
      ksm($this->getName(), 'no original segment');
      return NULL;
    }

    // multi-level inheritance
    if ($original_segment->hasParent()) {
      $original_segment = $original_segment->getOriginal();
    }

    if (empty($original_segment)) {
      ksm($this->getName(), 'no multi-level original segment');
      return NULL;
    }

    return $original_segment;
  }

  /**
   * Returns the AI prompt ID.
   *
   * @return int|string
   *   The AI prompt ID.
   */
  protected function getAIPromptId(): int|string {
    return $this->entity->id();
  }

  /**
   * Returns the AI prompt entity.
   *
   * @return mixed
   *   The AI prompt entity.
   */
  protected function getAIPromptEntity() {
    return $this->entity;
  }

  /**
   * Replaces tokens in the provided configuration array.
   *
   * @param array $config
   *   The configuration array.
   *
   * @return array
   *   The configuration array with tokens replaced.
   */
  protected function configurationReplaceTokens(array $config): array {
    $data = $this->getAvailableTokensData();
    $options = ['clear' => TRUE];

    foreach ($config as &$value) {
      if (is_string($value)) {
        $value = $this->tokenService->replace($value, $data, $options);
      } elseif (is_array($value)) {
        $value = $this->configurationReplaceTokens($value);
      }
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfigTokenized(): mixed {
    return $this->configurationReplaceTokens($this->getPluginConfig());
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginConfigValueTokenized(int|string $key): mixed {
    $value = $this->getPluginConfigValue($key);
    $config = $this->configurationReplaceTokens([$key => $value]);
    return $config[$key] ?? NULL;
  }

  /**
   * Get tokenized base configuration value.
   *
   * @param string $key
   *   The key of the configuration value.
   *
   * @return mixed
   *   The tokenized configuration value.
   */
  protected function getConfigurationValueTokenized(string $key): mixed {
    $value = $this->getConfigurationValue($key);
    $config = $this->configurationReplaceTokens([$key => $value]);
    return $config[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableTokensData(): array {
    $data = $this->tokenService->getInfo();
    $data['aiprompt'] = $this->getAIPromptEntity();
    $data['prompt-args'] = $data['aiprompt']->getArguments();

    foreach ($data['prompt-args'] as $key => $argument) {

      // Copy entity argument to parent to provide information what fieldnames
      // are available, @see issue https://www.drupal.org/project/ideas/issues/3461765
      if ($argument->getType() == 'entity') {
        $entity_type = $argument->getValue()->getEntityTypeId();
        $data[$entity_type] = $argument->getValue();
      }

      $data['tokens']['prompt-args'][$key] = [
        'name' => $key,
        'description' => $argument->getDescription()
      ];
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableTokensTreeBuildElement(): array {
    $token_types = array_keys($this->getAvailableTokensData());
    return [
      '#theme' => 'token_tree_link',
      '#token_types' => $token_types
    ];
  }

  /**
   * Helper to load object values.
   */
  protected function loadSegmentValues() {
    $this->set('status', (bool) $this->getConfigurationValue('status'));
    $this->set('weight', $this->getConfigurationValue('weight'));
    $this->set('dependency_data', $this->getConfigurationValue('dependency_data'));
  }

  /**
   * Retrieves plugin form base values.
   *
   * @param FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The base form values.
   */
  protected function getBaseFormValues(FormStateInterface $form_state) {
    $data_keys = $this->getBaseConfigKeys();
    $values = array_intersect_key($form_state->getValues(), array_flip($data_keys));

    if (!empty($values['dependency_data'])) {
      $values['dependency_data'] = json_decode($values['dependency_data'], TRUE);
    }

    return $values;
  }

  /**
   * Retrieves plugin form extended values.
   *
   * @param FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The plugin form values.
   */
  protected function getExtendedFormValues(FormStateInterface $form_state) {

    $values = $form_state->cleanValues()->getValues();

    // cleanup deep nested values
    if (is_array($values)) {
      $values = AIPromptArrayHelper::removeKeysStartingWithUnderscore($values);
    }

    $data_keys = $this->getPluginConfigKeys();

    return array_intersect_key($values, array_flip($data_keys));
  }

}
