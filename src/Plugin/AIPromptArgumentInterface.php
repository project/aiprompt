<?php

namespace Drupal\aiprompt\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines an interface for AI Prompt Argument plugins.
 */
interface AIPromptArgumentInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Defines default configuration of the plugin.
   * 
   * @return array
   */
  public function defineConfiguration(): array;

  /**
   * Gets default configuration of the plugin (both parent and children combined)
   * 
   * @return array
   */
  public function getDefaultConfiguration(): array;

  /**
   * Get configuration value.
   *
   * @param string $key
   *   The key of the configuration.
   *
   * @return mixed
   *   The configuration value.
   */
  public function getConfigurationValue(string $key): mixed;

  /**
   * Gets configuration keys.
   *
   * @return array
   */
  public function getConfigKeys(): array;

  /**
   * Build form for the plugin configuration.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array;

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void;

  /**
   * Validate the argument value.
   *
   * @param mixed $value
   *   The value to validate.
   *
   * @return bool
   *   TRUE if the value is valid, otherwise FALSE.
   */
  public function isValueValid(mixed $value): bool;

  /**
   * Validate the argument default value in form configuration.
   *
   * @param mixed $value
   *   The value to validate.
   *
   * @return bool
   *   TRUE if the value is valid, otherwise FALSE.
   */
  public function validateDefaultValue(mixed $value): bool;

  /**
   * Return the argument value.
   *
   * @return mixed
   */
  public function getValue(): mixed;

  /**
   * Set the argument value.
   *
   * @param mixed $value
   */
  public function setValue($value): void;

  /**
   * Get the type of the argument.
   *
   * @return string
   */
  public function getType(): string;

  /**
   * Get argument configuration (description).
   *
   * @return string
   */
  public function getDescription(): string;

  /**
   * Get a preview of argument value for display to the user.
   *
   * @return string
   */
  public function previewValue(): string;

}
