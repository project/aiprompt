<?php

namespace Drupal\aiprompt\Plugin;

use Drupal\aiprompt\Utility\AIPromptArrayHelper;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use InvalidArgumentException;

/**
 * Base class for AI Prompt Argument plugins.
 */
abstract class AIPromptArgumentBase extends PluginBase implements AIPromptArgumentInterface {

  use StringTranslationTrait;
  use DependencySerializationTrait;

  /**
  * The entity type manager.
  *
  * @var \Drupal\Core\Entity\EntityTypeManagerInterface
  */
  protected $entityTypeManager;

  /**
   * Argument value
   */
  public $value;

  /**
   * Parent AI Prompt entity.
   */
  public $entity;

  /**
   * Data about argument dependencies.
   */
  protected array $dependencies_data;

  /**
   * Is argument new (not yet saved to the database)
   */
  protected bool $is_new;

  /**
   * Constructs a new ExamplePluginBase.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  public function setEntity($entity) {
    $this->entity = $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(): mixed {
    return $this->value ?? $this->configuration['default_value'];
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($value): void {
    if (!$this->isValueValid($value)) {
      throw new \InvalidArgumentException(t(
        'The value for argument "@name" is invalid.',
        ['@name' => $this->configuration['name']]
      ));
    }
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function isValueValid(mixed $value): bool;

  /**
   * {@inheritdoc}
   */
  public function validateDefaultValue(mixed $value): bool {
    return $this->isValueValid($value);
  }

  /**
   * {@inheritdoc}
   */
  private function baseConfiguration(): array {
    return [
      'name' => [
        'default' => '',
      ],
      'default_value' => [
        'default' => ''
      ],
      'description' => [
        'default' => ''
      ]
    ];
  }

  /**
   * {@inheritdoc}
   */
  abstract public function defineConfiguration(): array;

  /**
   * {@inheritdoc}
   */
  public function getConfigKeys(): array {
    $default = $this->baseConfiguration() + $this->defineConfiguration();
    return array_keys($default);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultConfiguration(): array {
    $base_defaults = $this->baseConfiguration();
    $plugin_defaults = $this->defineConfiguration();

    $defaults = $base_defaults + $plugin_defaults;
    return AIPromptArrayHelper::getArrayColumn($defaults, 'default');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigurationValue(string $key): mixed {
    return $this->configuration[$key] ?? $this->getDefaultConfiguration()[$key];
  }

  /**
   * Get plugin label.
   */
  public function getPluginLabel() {
    return $this->getPluginDefinition()['label'];
  }

  /**
   * Get prompt argument name.
   *
   * @return string|null
   *   Segment name or null if it is not set.
   */
  public function getName(): ?string {
    return $this->getConfigurationValue('name');
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraNotes(): string {
    $parents = $this->getDependencies('parent');
    $output = '';
    foreach ($parents as $key => $parent) {
      $output .= $this->t('<em>(from @link)</em></br>', 
        ['@link' => $parent->getPromptLink()->toString()]
      );
    }
    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return $this->getPluginId();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(): string {
    return $this->getConfigurationValue('description');
  }

  /**
   * {@inheritdoc}
   */
  public function previewValue(): string {
    return $this->getValue();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $value = $this->configuration['name'] ?? FALSE;
    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Machine name'),
      '#description' => $this->t('Can contain lowercase letters, numbers, and underscores, and be no longer than 32 characters'),
      '#required' => TRUE,
      '#default_value' => $value,
      '#disabled' => (bool) $value,
      '#weight' => 5,
    ];

    $form['default_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Value'),
      '#default_value' => $this->getFormValue('default_value', $form_state),
      '#required' => TRUE,
      '#weight' => 10,
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->getFormValue('description', $form_state),
      '#weight' => 15,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {

    $name = $form_state->getValue('name');
    if (!preg_match('/^[a-z0-9_]+$/', $name) || mb_strlen($name) > 32) {
      $form_state->setErrorByName('name', $this->t('The name must only contain lowercase letters, numbers, and underscores, and be no longer than 32 characters.'));
    }
    if (empty($this->configuration['name'])) {
      $data = $this->entity->getArgumentData($name);
      if (!empty($data)) {
        $form_state->setErrorByName('name', $this->t("Argument with name '@name' already exists.", ['@name' => $name]));
      }
    }

    $default = $form_state->getValue('default_value');
    if ($default && !$this->validateDefaultValue($default)) {
      $form_state->setErrorByName('default_value', $this->t('The provided default value is not valid.'));
    }
  }

  /**
   * Get form value.
   */
  protected function getFormValue($key, FormStateInterface $form_state) {
    $value = $form_state->getValue($key);
    if (empty($value)) {
      $value = $this->getConfigurationValue($key);
    }
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies($type) {
    return $this->dependencies_data[$type] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function setDependency($type, $key, $data = []) {
    $this->dependencies_data[$type][$key] = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function hasDependency($type) {
    return !empty($this->dependencies_data[$type]);
  }

  /**
   * {@inheritdoc}
   */
  public function hasParent() {
    return $this->hasDependency('parent');
  }

  /**
   * {@inheritdoc}
   */
  public function isNew(): bool {
    return $this->is_new ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setNew(): void {
    $this->is_new = TRUE;
  }

 /**
   * {@inheritdoc}
   */
  public function isActionAvailable(string $action): bool {
    if (! $this->hasParent()) {
      return TRUE;
    }

    return FALSE;
  }

}
