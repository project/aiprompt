<?php

namespace Drupal\aiprompt\Plugin;

use Drupal\aiprompt\Annotation\AIPromptSegment;
use Drupal\aiprompt\Plugin\AIPromptSegmentBase;
use Drupal\aiprompt_content\Entity\AIPrompt;
use Drupal\Component\Utility\Html;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;

/**
 * For extending by ContentPrompt and ConfigPrompt entity segment plugins, because they share similar code.
 */
class PromptEntitySegmentBase extends AIPromptSegmentBase {

  const AJAX_WRAPPER = "segment-sub-form";

  /**
   * {@inheritdoc}
   */
  public function defineConfiguration(): array {
    return [
      'method' => ['default' => 'entity_autocomplete'],
      'entity_id' => ['default' => NULL],
      'include_method' => ['default' => 'render'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render(): string {
    if ($this->hasExtraSegments()) {
      return '';
    }

    $prompt = $this->getPrompt();

    $output = '';
    if ($prompt) {
      $output .= $prompt->toString();
    }

    return $output;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPreview(): array {
    $build = [];

    $link = $this->getPromptLink();

    if ($this->hasExtraSegments()) {
      $build['markup'] = [
        '#type' => 'markup',
        '#markup' => $this->t(
          '<em>(parent prompt)</em></br> Provides segments of parent prompt "@link"',
          ['@link' => $link->toString()]
        ),
        '#allowed_tags' => ['br', 'em']
      ];
    } else {
      $link = $link->toRenderable();
      $link['#suffix'] = '</br>';
      $build['link'] = $link;
      $preview_string = $this->preview();
      $build['markup'] = [
        '#type' => 'markup',
        '#markup' => nl2br(Html::escape($preview_string)),
        '#allowed_tags' => ['br']
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {

    $ajax = [
      'wrapper' => self::AJAX_WRAPPER,
      'callback' => [$this, 'ajax'],
    ];

    $values = $this->getFormValues($form_state);

    $form['method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Method to reference prompt'),
      '#options' => [
        'entity_autocomplete' => $this->t('Autocomplete widget'),
        'tokens' => $this->t('Tokens')
      ],
      '#default_value' => $values['method'],
      '#ajax' => $ajax,
      '#required' => TRUE,
      '#attributes' => ['autocomplete' => ['off']]
    ];

    if ($values['method'] == 'entity_autocomplete') {
      $form['entity_id'] = [
        '#type' => 'entity_autocomplete',
        '#title' => $this->t('Prompt'),
        '#required' => TRUE,
        '#target_type' => $this->entity_type,
        '#default_value' => $values['entity_id'],
        '#attributes' => ['autocomplete' => ['off']],
        '#description' => $this->t('Prompt entity suggestions will appear as you type.'),
      ];
    }
    elseif ($values['method'] == 'tokens') {
      $form['entity_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Prompt'),
        '#required' => TRUE,
        '#default_value' => $values['entity_id'],
        '#attributes' => ['autocomplete' => ['off']],
        '#description' => $this->t('Provide prompt ID with tokens.'),
      ];
      $form['entity_id_tokens'] = $this->getAvailableTokensTreeBuildElement();
    }

    $form['include_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('How to include prompt?'),
      '#options' => [
        'render' => $this->t('Render prompt output'),
        'extend' => $this->t('Include all parent segments individually and allow to edit them')
      ],
      '#default_value' => $values['include_method'],
      '#required' => TRUE,
      '#attributes' => ['autocomplete' => ['off']]
    ];

    $form['#prefix'] = '<div id="'.self::AJAX_WRAPPER.'">';
    $form['#suffix'] = '</div>';

    $form = parent::buildConfigurationForm($form, $form_state);

    return $form;
  }

  /**
   * Helper to get prompt link
   */
  public function getPromptLink() {
    $prompt = $this->getPrompt();
    return $prompt->toLink(NULL, 'edit-form');
  }

  /**
   * {@inheritdoc}
   */
  public function isDraggable(): bool {
    return !$this->hasExtraSegments();
  }

  /**
   * {@inheritdoc}
   */
  public function hasExtraSegments() {
    return $this->getPluginConfigValue('include_method') === 'extend';
  }

  /**
   * {@inheritdoc}
   */
  public function hasExtraArguments() {
    return $this->hasExtraSegments();
  }

  /**
   * {@inheritdoc}
   */
  public function provideExtraSegments() {
    $prompt = $this->getPrompt();
    return $prompt->getEnabledSegments();
  }

  /**
   * {@inheritdoc}
   */
  public function provideExtraArguments() {
    $prompt = $this->getPrompt();
    return $prompt->getArguments();
  }

  /**
   * {@inheritdoc}
   */
  public function getExtraSegment($segment_id) {
    $prompt = $this->getPrompt();
    $segments = $prompt->getEnabledSegments();
    return $segments[$segment_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    if ($this->hasExtraSegments()) {
      return -100;
    }
    return $this->weight;
  }

  /**
   * Ajax callback.
   */
  public function ajax(array $form, FormStateInterface $form_state) {
    $selector = '#'.self::AJAX_WRAPPER;

    $response = new AjaxResponse();

    // Replace wrapper with rebuilt form
    $response->addCommand(new ReplaceCommand($selector, $form));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    if ($form_state->getValue('entity_id') == $this->getAIPromptId()) {
      $form_state->setErrorByName('entity_id', $this->t('Prompt cannot reference itself'));
    }
  }

  /**
   * Get default values for the form.
   */
  private function getFormValues(FormStateInterface $form_state): array {
    $values = [];
    foreach (array_keys($this->defineConfiguration()) as $key) {
      if ($key == 'entity_id' && $values['method'] == 'entity_autocomplete') {
        $values[$key] = $this->getPrompt();
      } else {
        $values[$key] = $form_state->getValue($key) ?? $this->getPluginConfigValue($key);
      }
    }
    return $values;
  }

}
