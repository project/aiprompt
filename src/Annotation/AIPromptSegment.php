<?php

namespace Drupal\aiprompt\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an AI Prompt segment plugin annotation object.
 *
 * @Annotation
 */
class AIPromptSegment extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   * 
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   * 
   * @ingroup plugin_translatable
   */
  public $description;
}