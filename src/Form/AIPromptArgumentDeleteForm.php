<?php

namespace Drupal\aiprompt\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AIPromptArgumentDeleteForm.
 *
 * Provides a confirm form for deleting the argument from a Prompt.
 */
class AIPromptArgumentDeleteForm extends ConfirmFormBase {

  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  protected $entity_type;
  protected $entity;
  protected $argument;

  /**
   * Constructs a new Form.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $currentRouteMatch) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * Access check.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(Request $request, AccountInterface $account) {
    $this->load();

    // Primary access check is defined in routing.yml file.
    // This additional access check here forbids access if specific action is disabled.
    if (! $this->argument->isActionAvailable('delete')) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aiprompt_argument_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $this->load();

    return $this->t('Are you sure you want to delete argument @name?', 
      ['@name' => $this->argument->getName()]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $this->load();

    return new Url("entity.{$this->entity_type}.edit_form", [$this->entity_type => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->load();

    $this->entity->deleteArgument($this->argument->getName());
    $this->entity->save();

    $this->messenger()->addStatus(
      $this->t('Argument @name has been deleted.', [
        '@name' => $this->argument->getName(),
      ])
    );

    $form_state->setRedirect("entity.{$this->entity_type}.edit_form", [$this->entity_type => $this->entity->id()]);
  }

  /**
   * Loads necessary data for the form.
   */
  public function load() {
    if (!empty($this->entity)) return;

    $this->entity_type = 'aiprompt';
    $this->entity = $this->currentRouteMatch->getParameter('aiprompt');

    if (empty($this->entity)) {
      $this->entity_type = 'aiprompt_config';
      $this->entity = $this->currentRouteMatch->getParameter('aiprompt_config');
    }

    $argument_name = $this->currentRouteMatch->getParameter('argument_name');
    $this->argument = $this->entity->getArgument($argument_name);
  }

}
