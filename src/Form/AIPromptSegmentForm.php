<?php

namespace Drupal\aiprompt\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class AIPromptSegmentForm extends FormBase {

  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  protected $entity_type;
  protected $entity;
  protected $segment;

  /**
   * Constructs a new AIPromptArgumentForm object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $currentRouteMatch) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * Access check.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(Request $request, AccountInterface $account) {
    $this->load();

    // Primary access check is defined in routing.yml file.
    // This additional access check here forbids access if specific action is disabled.
    if (! $this->segment->isActionAvailable('edit')) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

  /**
   * Returns the title for the form.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  public function getTitle() {
    $this->load();
    $label = $this->segment->getPluginLabel();

    if ($this->segment->isNew()) {
      return $this->t('Add <em>@label</em> prompt segment', ['@label' => $label]);
    } else {
      return $this->t('Edit <em>@label</em> prompt segment', ['@label' => $label]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aiprompt_plugin_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->load();

    $form = $this->segment->buildConfigurationForm($form, $form_state);

    $form['actions'] = [
      '#type' => 'container',
      '#weight' => 10
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->load();

    $this->segment->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->load();

    $this->segment->submitConfigurationForm($form_state);

    $form_state->setRedirect("entity.{$this->entity_type}.edit_form", [$this->entity_type => $this->entity->id()]);
  }

  /**
   * Loads necessary data for the form.
   *
   * This method loads the entity, segment data, plugin ID, and plugin instance.
   */
  protected function load() {
    if (!empty($this->entity)) return;

    $this->entity_type = 'aiprompt';
    $this->entity = $this->currentRouteMatch->getParameter('aiprompt');

    if (empty($this->entity)) {
      $this->entity_type = 'aiprompt_config';
      $this->entity = $this->currentRouteMatch->getParameter('aiprompt_config');
    }

    $segment_id = $this->currentRouteMatch->getParameter('segment_id');
    $plugin_id = $this->currentRouteMatch->getParameter('plugin_id');

    $this->segment = $this->entity->getSegment($segment_id, $plugin_id);
  }

}
