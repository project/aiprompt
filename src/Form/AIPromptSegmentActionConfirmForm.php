<?php

namespace Drupal\aiprompt\Form;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AIPromptSegmentActionConfirmForm.
 *
 * Provides a confirm form for performing an action of prompt segment from a Prompt.
 */
class AIPromptSegmentActionConfirmForm extends ConfirmFormBase {

  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  protected $entity_type;
  protected $entity;
  protected $segment;

  /**
   * Constructs a new Form.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $currentRouteMatch) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * Access check.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(Request $request, AccountInterface $account) {
    $this->load();

    if (! in_array($this->action, ['delete', 'restore', 'clone'])) {
      return AccessResult::forbidden();
    }

    // check content entity access
    if ($this->entity_type == 'aiprompt') {
      if ($this->action == 'delete' && !$this->entity->access('delete', $account)) {
        return AccessResult::forbidden();
      } elseif (!$this->entity->access('edit', $account)) {
        return AccessResult::forbidden();
      }

    // check config entity access
    } elseif (!$account->hasPermission('administer aiprompt configurations')) {
      return AccessResult::forbidden();
    }

    // check segment action availability
    if (! $this->segment->isActionAvailable($this->action)) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aiprompt_segment_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $this->load();

    return $this->t('Are you sure you want to @action segment @name?', [
      '@name' => $this->segment->getPluginLabel(),
      '@action' => $this->t($this->action)
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    $this->load();

    return new Url("entity.{$this->entity_type}.edit_form", [$this->entity_type => $this->entity->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    if (in_array($this->action, ['clone'])) {
      return $this->t('New prompt segment will be created.');
    }
    return $this->t('This action cannot be undone.');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->load();

    if ($this->action == 'clone') {
      $this->entity->cloneSegment($this->segment->getName());
    } else {
      $this->entity->deleteSegment($this->segment->getName());
    }
    $this->entity->save();

    $action = $this->action.'d'; 

    $this->messenger()->addStatus(
      $this->t('Segment @name has been @action.', [
        '@name' => $this->segment->getPluginLabel(),
        '@action' => $this->t($action),
      ])
    );

    $form_state->setRedirect("entity.{$this->entity_type}.edit_form", [$this->entity_type => $this->entity->id()]);
  }

  /**
   * Loads necessary data for the form.
   */
  public function load() {
    if (!empty($this->entity)) return;

    $this->entity_type = 'aiprompt';
    $this->entity = $this->currentRouteMatch->getParameter('aiprompt');

    if (empty($this->entity)) {
      $this->entity_type = 'aiprompt_config';
      $this->entity = $this->currentRouteMatch->getParameter('aiprompt_config');
    }

    $segment_id = $this->currentRouteMatch->getParameter('segment_id');
    $this->segment = $this->entity->getSegment($segment_id);

    $this->action = $this->currentRouteMatch->getParameter('action');
  }

}
