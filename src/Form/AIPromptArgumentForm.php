<?php

namespace Drupal\aiprompt\Form;

use Drupal\aiprompt\Utility\AIPromptArrayHelper;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

class AIPromptArgumentForm extends FormBase {

  use DependencySerializationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  protected $entity_type;
  protected $entity;
  protected $argument;

  /**
   * Constructs a new AIPromptArgumentForm object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CurrentRouteMatch $currentRouteMatch) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentRouteMatch = $currentRouteMatch;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * Access check.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account object.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result.
   */
  public function access(Request $request, AccountInterface $account) {
    $this->load();

    // Primary access check is defined in routing.yml file.
    // This additional access check here forbids access if specific action is disabled.
    if (! $this->argument->isActionAvailable('edit')) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle() {
    $this->load();
    $label = $this->argument->getPluginLabel();

    if ($this->argument->isNew()) {
      return $this->t('Add @type prompt argument', ['@type' => $label]);
    } else {
      return $this->t('Edit <em>@name</em> @type prompt argument', [
        '@name' => $this->argument->getName(),
        '@type' => $label,
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aiprompt_argument_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->load();

    $form = $this->argument->buildConfigurationForm($form, $form_state);

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 20
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->load();
    $this->argument->validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->load();

    $values = $this->getFormValues($form_state);
    $values['type'] = $this->argument->getType();

    $this->entity->updateArgumentData($values, $values['name']);
    $this->entity->save();

    $form_state->setRedirect("entity.{$this->entity_type}.edit_form", [$this->entity_type => $this->entity->id()]);
  }

  /**
   * Retrieves plugin form main values.
   *
   * @param FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The base form values.
   */
  public function getFormValues(FormStateInterface $form_state) {
    $data_keys = $this->argument->getConfigKeys();
    return array_intersect_key($form_state->getValues(), array_flip($data_keys));
  }

  /**
   * Loads necessary data for the form.
   */
  protected function load() {
    if (!empty($this->entity)) return;

    $this->entity_type = 'aiprompt';
    $this->entity = $this->currentRouteMatch->getParameter('aiprompt');

    if (empty($this->entity)) {
      $this->entity_type = 'aiprompt_config';
      $this->entity = $this->currentRouteMatch->getParameter('aiprompt_config');
    }

    $argument_name = $this->currentRouteMatch->getParameter('argument_name');
    $plugin_id = $this->currentRouteMatch->getParameter('plugin_id');

    $this->argument = $this->entity->getArgument($argument_name, $plugin_id);
  }
}
