<?php

namespace Drupal\aiprompt;

/**
 * Provides an interface for managining nested data.
 *
 * @ingroup aiprompt
 */
interface NestedDataInterface {

  /**
   * Gets a value in a nested array with variable depth.
   * 
   * @param mixed $key
   *   If $key is a string, it will return configuration value by this key.
   *   If $key is an array, each element of the array will be used as a nested key starting with the outermost key.
   *   For example, $fieldname = ['foo', 'bar'] will return $configuration['foo']['bar'].
   * 
   * @return mixed
   *   The data value for the requested key. NULL if the key does not exist.
   */
  public function getDataNestedValue(mixed $key): mixed;

  /**
   * Sets a value in a nested array with variable depth.
   * 
   * @param mixed $key
   *   An array of parent keys, starting with the outermost key. Or single key string.
   * @param mixed $value
   *   The data value to set. If NULL is provided, key will be unset.
   */
  public function setDataNestedValue(mixed $key, mixed $value): void;
}