<?php

namespace Drupal\aiprompt;

/**
 * Provides an interface defining common methods between AI prompt entities.
 */
interface AIPromptInterface  {

  /**
   * Get complete prompt output to string.
   *
   * @return string
   */
  public function toString(): string;

  /**
   * Preview prompt output
   *
   * @return string
   *   Preview string with added HTML line breaks and escaped.
   */
  public function buildPreview(): array;

  /**
   * Returns prompt segments.
   *
   * @return array
   *   Array containing prompt segments.
   */
  public function getSegments(): array;

  /**
   * Returns prompt segments sorted by weight.
   *
   * @return array
   *   Array containing prompt segments.
   */
  public function getSegmentsSortedByWeight(): array;

  /**
   * Returns enabled prompt segments sorted by weight.
   *
   * @return array
   *   Array containing prompt segments.
   */
  public function getEnabledSegmentsSortedByWeight(): array;

  /**
   * Returns a segment.
   *
   * @param string|null $machine_name
   *   The machine name of the segment. If not provided, new empty segment will be returned.
   * @param string|null $plugin_id
   *   If segment does not exist and plugin_id is provided, new segment with default values will be returned.
   *
   * @return object
   *   Segment plugin instance.
   */
  public function getSegment(?string $machine_name, ?string $plugin_id = NULL): mixed;

  /**
   * Delete segment.
   *
   * @param string $machine_name
   *   The machine name of the segment.
   */
  public function deleteSegment(string $machine_name): void;

  /**
   * Clone segment.
   *
   * @param string $machine_name
   *   The machine name of the segment.
   */
  public function cloneSegment(string $machine_name): void;

  /**
   * Returns prompt segments.
   *
   * @return array
   *   Array containing prompt segments data.
   */
  public function getSegmentsData(): array;

  /**
   * Sets prompt segments.
   *
   * @param array $segments
   *   Array containing prompt segments data.
   */
  public function setSegmentsData(array $segments): void;

  /**
   * Sets a segment.
   *
   * @param array $segment
   *   Array containing segment data.
   * @param string|null $machine_name
   *   The machine name of the segment. If not provided, a new one will be generated.
   */
  public function setSegmentData(array $segment, $machine_name = NULL): void;

  /**
   * Returns a segment.
   *
   * @param string|null $machine_name
   *   The machine name of the segment. If not provided, new empty segment will be returned.
   * @return array
   *   Array containing segment data.
   */
  public function getSegmentData($machine_name = NULL): array;

  /**
   * Deletes a segment.
   *
   * @param string $machine_name
   *   The machine name of the segment to delete.
   */
  public function deleteSegmentData(string $machine_name): void;

  /**
   * Save weights of segments.
   * 
   * @param array $weights
   *   Value of values from form weights element.
   */
  public function updateSegmentsFromSubmittedData($submitted_data): void;

  /**
   * Get data of all arguments.
   *
   * @return array
   */
  public function getArgumentsData(): array;

  /**
   * Get data of an argument.
   *
   * @param string|null $name
   *   The machine name of the argument. If not provided, new empty argument data will be returned.
   * @return array
   *   Array containing argument data.
   */
  public function getArgumentData(string $name = NULL): array;

  /**
   * Set or update argument data.
   *
   * @param array $data
   *   The data for the argument.
   * @param string $name
   *   The name of the argument.
   */
  public function updateArgumentData(array $data, string $name): void;

  /**
   * Remove argument data.
   *
   * @param string $name
   *   The name of the argument.
   */
  public function deleteArgumentData(string $name): void;

  /**
   * Set argument value.
   *
   * @param string $name
   *   The name of the argument.
   * @param mixed $data
   *   The value of the argument.
   */
  public function setArgumentValue(string $name, mixed $value): void;

  /**
   * Get argument value.
   *
   * @param string $name
   *   The name of the argument.
   *
   * @return mixed
   *   Argument value
   */
  public function getArgumentValue(string $name): mixed;

  /**
   * Get all argument objects.
   *
   * @return array
   *   Loaded argument plugin objects.
   */
  public function getArguments(): array;

  /**
   * Get argument object.
   *
   * @param string $name
   *   The name of the argument.
   *
   * @return mixed
   *   Get specific argument object.
   */
  public function getArgument(string $key): mixed;

}
